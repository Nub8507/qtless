#ifndef NAMEVALIDATOR_H
#define NAMEVALIDATOR_H

#include<QValidator>

class NameValidator : public QValidator
{
public:
    NameValidator(QObject* parent):QValidator(parent)
    {
    }

    virtual State validate(QString& str,int& pos) const;
};

#endif // NAMEVALIDATOR_H
