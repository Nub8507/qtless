#include "staticfunctions.h"

#include <regex>
#include <QString>
#include <QStringList>
#include <QRegExp>

using namespace StaticFunctions;
std::string StaticFunctions::FindCharset(std::string str)
{
    //
    //
    std::regex reg("charset=[ \"]*(.*?)[ \">]") ;
    std::smatch m;
    bool result=regex_search(str,m,reg);
    //
    if(result){
        //
        std::string t=m[1].str();
        return t;
        //
    }
    //
    return "Windows-1251";
    //
}

void StaticFunctions::RemoveTags(QString& source)
{
    //
    Range pos;
    //
    while (pos.StartPos!=-1||pos.EndPos!=-1){
        pos.StartPos=source.indexOf("<",0,Qt::CaseInsensitive);
        pos.EndPos=source.indexOf(">",0,Qt::CaseInsensitive);
        if(pos.EndPos<pos.StartPos)source.remove(0,pos.EndPos+1);
        if(pos.EndPos>pos.StartPos)source.remove(pos.StartPos,pos.lenght());
    }
    //
}

//Разбить полученную строку на подстроки
inline QStringList StrToList(QString& source){return source.split("\n");}

//Найти автора произведений, если не найден вернуть пустую строку
QString FindAuthor(QStringList& List)
{
    const int NumOfAuthorString=8;
    if(List.size()<NumOfAuthorString+1)return "";
    //
    QString t=List[NumOfAuthorString];
    //
    int StartPos=t.indexOf("<h3>");
    if(StartPos==-1)return "";
    //
    int EndPos=t.indexOf(":<",StartPos+4);
    if (EndPos==-1)return "";
    //
    return t.mid(StartPos+4,EndPos-(StartPos+4));
    //
}

//Найти блок списка произведений
Range FindTextList(const QStringList& List){
    //
    StaticFunctions::Range t;
    //
    t.StartPos=List.indexOf(QRegExp{"<!--------- Блок ссылок на произведения ------------>\r"});
    t.EndPos=List.indexOf(QRegExp{"<!--------- Подножие ------------------------------->\r"});
    //
    return t;
    //
}

QString ParceAnotation(QString source)
{
    Range pos;
    pos.StartPos=0;
    pos.EndPos=source.indexOf("<DD>",0,Qt::CaseInsensitive);
    QString t="";
    while (pos.EndPos!=-1){
        t+=ParceAnotation(source.mid(pos.StartPos,pos.lenght()))+"\n";
        pos.swap(4);
        pos.EndPos=source.indexOf("<DD>",pos.StartPos,Qt::CaseInsensitive);
    }
    pos.EndPos=source.indexOf("/img/",pos.StartPos,Qt::CaseInsensitive);
    if (pos.EndPos==-1)return t==""?source:t;
    return t;
}

void ParceBookString(QString& source,QList<ContentElement>& DataList,QString Author,QString HTMLAuthorAdress,QString Group,QString HTMLGroupAdress)
{
    //
    Range pos;
    pos.StartPos=source.indexOf("HREF=",0,Qt::CaseInsensitive);
    if (pos.StartPos==-1)return;
    pos.EndPos=source.indexOf(">",pos.StartPos,Qt::CaseInsensitive);
    if (pos.EndPos==-1)return;
    pos.StartPos+=5;
    QString HTMLContentAdress=source.mid(pos.StartPos,pos.lenght());
    //
    pos.swap(1);
    //
    pos.EndPos=source.indexOf("</A>",pos.StartPos,Qt::CaseInsensitive);
    if (pos.EndPos==-1)return;
    //
    QString Content=source.mid(pos.StartPos,pos.lenght());
    RemoveTags(Content);
    pos.swap();
    //
    pos.EndPos=source.indexOf("<DD>",pos.StartPos,Qt::CaseInsensitive);
    QString Anotation="";
    if (pos.EndPos!=-1){
        pos.swap(4);
        pos.EndPos=source.indexOf("</DL>",pos.StartPos,Qt::CaseInsensitive);
        if(pos.EndPos!=-1)Anotation=ParceAnotation(source.mid(pos.StartPos,pos.lenght()));
    }
    RemoveTags(Anotation);
    //
    DataList.push_back({Author,HTMLAuthorAdress,Group,HTMLGroupAdress,Content,HTMLContentAdress,Anotation,QDate()});
    //
}

void ParceGroupString(QString s1,QString s2,QList<ContentElement>& DataList,QString Author,QString HTMLAuthorAdress,QString& Group,QString& HTMLGroupAdress)
{
    //
    Range pos;
    pos.EndPos=s1.indexOf("<a name=gr",0,Qt::CaseInsensitive);
    if (pos.EndPos==-1)return;
    //
    pos.swap();
    pos.EndPos=s1.indexOf(">",pos.StartPos,Qt::CaseInsensitive);
    pos.swap(1);
    //
    pos.EndPos=s1.indexOf("<a href=",pos.StartPos,Qt::CaseInsensitive);
    HTMLGroupAdress="";
    if(pos.EndPos!=-1){
        pos.swap(8);
        pos.EndPos=s1.indexOf(">",pos.StartPos,Qt::CaseInsensitive);
        if(pos.EndPos!=-1)HTMLGroupAdress=s1.mid(pos.StartPos,pos.lenght());
        pos.swap(1);
    }
    //
    pos.EndPos=s1.indexOf("</a>",pos.StartPos,Qt::CaseInsensitive);
    if (pos.EndPos==-1)pos.EndPos=s1.indexOf("<gr",pos.StartPos,Qt::CaseInsensitive);
    if(pos.EndPos==-1)pos.EndPos=s1.length();
    //
    Group=s1.mid(pos.StartPos,pos.lenght());
    RemoveTags(Group);
    //
    if(HTMLGroupAdress.left(6)=="/type/")HTMLGroupAdress="";
    //
    RemoveTags(s2);
    DataList.push_back({Author,HTMLAuthorAdress,Group,HTMLGroupAdress,"","",s2,QDate()});
    //
    if(HTMLGroupAdress!="");//здесь должено быть рекурсивое получение страницы и обработка
    //
}

void ParceMainPage(const QStringList& List,StaticFunctions::Range Range,QList<ContentElement>& DataList,QString Author,QString HTMLAuthorAdress,QString Group)
{
    auto StartPos{List.begin()+Range.StartPos+1};
    auto EndPos{List.begin()+Range.EndPos};
    QString HTMLGroupAdress="";
    for(auto pos=StartPos;pos!=EndPos;++pos){
        QString t=*pos;
        if(t.left(8)=="</small>"){ QString t2=*(pos+=2); ParceGroupString(t,t2,DataList,Author,HTMLAuthorAdress,Group,HTMLGroupAdress);};
        if(t.left(8)=="<DL><DT>") ParceBookString(t,DataList,Author,HTMLAuthorAdress,Group,HTMLGroupAdress);
    }

}

QList<ContentElement> StaticFunctions::ParceContents(QString source, QString HTMLAdress)
{
    //
    QStringList List=StrToList(source);
    //
    QString Author=FindAuthor(List);
    RemoveTags(Author);
    //
    Range ContentsList=FindTextList(List);
    //
    QList<ContentElement> DataList;
    //
    ParceMainPage(List,ContentsList,DataList,Author,HTMLAdress,"");
    //

    //
    return DataList;
    //
}
