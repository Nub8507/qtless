#ifndef WEBBROWSER_H
#define WEBBROWSER_H

#include "authortreemodel.h"

#include <QWidget>
#include <QUrl>
#include <QNetworkReply>
#include <contentelement.h>
#include <QTreeView>

class QLineEdit;
class QNetworkAccessManager;
class QPushButton;
class QTextEdit;
class QScrollArea;
class QLabel;
class QPlainTextEdit;


class WebBrowser : public QWidget
{
    Q_OBJECT
public:
    explicit WebBrowser(QWidget *parent = nullptr);
    ~WebBrowser();

    QString getHTML();

private:

    QScrollArea* pSa;
    QLineEdit* ptxtUrl;
    QNetworkAccessManager* pHtml;
    QPushButton* pbtnUrl;
    QPushButton* pbtnFile;
    QPushButton* pbtnFile2;
    QPlainTextEdit* ptxtHtml;
    QList<ContentElement>t{};
    QTreeView* TV;
    AuthorTreeList* T;
    AuthorTreeModel* TVModel;
    //
    bool downloading;
    //
    QByteArray FindCharset(QByteArray& str);
    //
signals:
    void sendHTML(QString);

public slots:

private slots:
//    void slotFinished(QNetworkReply*);
    void slotFinished();
    void btnUrlClick();
    void btnFileClick();
    void btnFile2Click();

};

#endif // WEBBROWSER_H
