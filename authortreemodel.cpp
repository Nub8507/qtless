#include "authortreemodel.h"

AuthorTreeModel::AuthorTreeModel(AuthorTreeList* data, QObject *parent)
    :QAbstractItemModel(parent)
{
    this->rootItem=data;
}

AuthorTreeModel::~AuthorTreeModel()
{
    return;
}

QVariant AuthorTreeModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid()) {
        return QVariant();
    }

    if(role!=Qt::DisplayRole)
        return QVariant();
    //
    int column=index.column();
    //
    switch (column) {
	case 0:												//название
        return dataColumn(index,0);
	case 1:												//описание
        return dataColumn(index,1);
	case 2:												//HTML адрес
        return dataColumn(index,2);
	case 3:												//флаг обновления
        return dataColumn(index,3);
	case 4:												//есть дочерние элементы
		return dataColumn(index,4);
	case 5:												//Это элемент верхнего уровня
		return dataColumn(index,5);
	default:
        return QVariant();
    }
}

QVariant AuthorTreeModel::dataColumn(const QModelIndex &index, int column) const
{
    if (!index.isValid()) {
        return QVariant();
    }
    //
    AuthorTreeList *Item;
    Item = static_cast<AuthorTreeList*>(index.internalPointer());
    //
	#ifdef QT_DEBUG
		auto d1=Item->data();
	#endif
    switch (column) {
	case 0:														//название
        return QVariant(Item->data().Group);
	case 1:														//описание
        return QVariant(Item->data().Anotation);
	case 2:														//HTML адрес
        return QVariant(Item->data().HTMLAdress);
	case 3:														//флаг обновления
		if(Item->getUserCheck()<Item->getLastUpdate())
			return QVariant(true);
		else
			return QVariant(false);
	case 4:														//есть дочерние элементы
		return QVariant(Item->childCount()>0);
	case 5:														//Это элемент верхнего уровня
		return QVariant(Item->parent()==this->rootItem);
	default:
        return QVariant();
    }
}

Qt::ItemFlags AuthorTreeModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return 0;

    return QAbstractItemModel::flags(index);
}

QHash<int, QByteArray> AuthorTreeModel::roleNames() const
{
    QHash<int, QByteArray> roles = QAbstractItemModel::roleNames();
    roles[AuthorRole] = "author";
    roles[AnotationRole] = "anotation";

    return roles;
}

QVariant AuthorTreeModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    Q_UNUSED(section);

    if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
        return QVariant("rootItem");

    return QVariant();
}

QModelIndex AuthorTreeModel::index(int row, int column, const QModelIndex &parent) const
{
    if (!hasIndex(row, column, parent))
        return QModelIndex();

    AuthorTreeList *parentItem;

    if (!parent.isValid())
        parentItem = rootItem;
    else
        parentItem = static_cast<AuthorTreeList*>(parent.internalPointer());

    AuthorTreeList *childItem = parentItem->child(row);
	if (childItem){
#ifdef QT_DEBUG
		auto t1= createIndex(row, column, childItem);
		auto t2= static_cast<AuthorTreeList*>(t1.internalPointer());
#endif
        return createIndex(row, column, childItem);
	}
    else
        return QModelIndex();
}

QModelIndex AuthorTreeModel::parent(const QModelIndex &index) const
{

    if (!index.isValid())
        return QModelIndex();

    AuthorTreeList *childItem = static_cast<AuthorTreeList*>(index.internalPointer());
    AuthorTreeList *parentItem = childItem->parent();

    if (parentItem == rootItem)
        return QModelIndex();

    return createIndex(parentItem->row(), 0, parentItem);
}

int AuthorTreeModel::rowCount(const QModelIndex &parent) const
{
    AuthorTreeList *parentItem;
    if (parent.column() > 0)
        return 0;

    if (!parent.isValid())
        parentItem = rootItem;
    else
        parentItem = static_cast<AuthorTreeList*>(parent.internalPointer());

    return parentItem->childCount();
}

int AuthorTreeModel::columnCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return static_cast<AuthorTreeList*>(parent.internalPointer())->columnCount();
    else
		return rootItem->columnCount();
}

bool AuthorTreeModel::removeRows(int row, int count, const QModelIndex &parent)
{
	//
	bool rez=true;
	//
	if (parent.isValid()){
		AuthorTreeList* Item=static_cast<AuthorTreeList*>(parent.internalPointer());
		emit beginRemoveRows(parent,row,row+count-1);
			//
			for(int i=row+count-1;i>=row;--i)
			{
				if(Item->child(i)->childCount()>0){
					rez=rez&&removeRows(0,Item->child(i)->childCount(),createIndex(0,0,Item->child(i)));
					if(rez)
						rez=rez&&Item->del(Item->child(i));
				}
				else
					rez=rez&&Item->del(Item->child(i));
			}
			//
		emit endRemoveRows();
	}
	else{
		AuthorTreeList* Item=rootItem;
		emit beginRemoveRows(QModelIndex(),row,row+count-1);
			//
			for(int i=row+count-1;i>=row;--i)
			{
				if(Item->child(i)->childCount()>0){
					rez=rez&&removeRows(0,Item->child(i)->childCount(),createIndex(0,0,Item->child(i)));
					if(rez)
						rez=rez&&Item->del(Item->child(i));
				}
				else
					rez=rez&&Item->del(Item->child(i));
			}
			//
		emit endRemoveRows();
	}
	//
	return rez;
}

AuthorTreeList* AuthorTreeModel::add(const ContentElement &data)
{
	//
	AuthorTreeList *pp=rootItem->FindParent(data);
	//

	//
	if(pp==nullptr){
		emit beginInsertRows(QModelIndex(),rootItem->childCount(),rootItem->childCount());
		pp=rootItem->appendChild(data);
	}else {
		emit beginInsertRows(createIndex(pp->row(),0,pp),pp->childCount(),pp->childCount());
		pp=pp->appendChild(data);
	}
	//
	emit endInsertRows();
	//
	return pp;
	//
}

void AuthorTreeModel::add(const AuthorTreeList::NodeItem &data)
{
	AuthorTreeList* pp=add(data.source);
	pp->setUserCheck(data.getUserCheck());
	pp->setLastUpdate(data.getLastUpdate());
	pp->setDeleted(false);
	QModelIndex index=createIndex(pp->row(),0,pp);
	emit dataChanged(index,index);
}

void AuthorTreeModel::setTextUpdated(AuthorTreeList* data,const QDate NewDate)
{
	//
	data->setLastUpdate(NewDate);
	QModelIndex index=createIndex(data->row(),0,data);
	emit dataChanged(index,index);
	//
	setParentTextUpdateStatus(data->parent(),NewDate);
	//
}

void AuthorTreeModel::clear()
{
	//
	emit beginResetModel();
	rootItem->clear();
	emit endResetModel();
	//
}

void AuthorTreeModel::changeGroupUserCheckStatus(AuthorTreeList* data,const QDate NewDate)
{
	//
	data->setUserCheck(NewDate);
	QModelIndex index=createIndex(data->row(),0,data);
	emit dataChanged(index,index);
	//
	for(auto i=0;i<data->childCount();++i){
		setChildUserCheckStatus(data->child(i),NewDate);
	}
	//
	setParentUserCheckStatus(data->parent(),NewDate);
	//
}


QModelIndex AuthorTreeModel::findIndexData(AuthorTreeList *data)
{
	//
    if(data==nullptr||data->parent()==nullptr)return QModelIndex();
	//
	return index(data->row(),0,findIndexData(data->parent()));
	//
}

void AuthorTreeModel::setChildUserCheckStatus(AuthorTreeList *Item, const QDate NewDate)
{
	//
	Item->setUserCheck(NewDate);
	QModelIndex index=createIndex(Item->row(),0,Item);
	emit dataChanged(index,index);
	//
	for(auto i=0;i<Item->childCount();++i){
		setChildUserCheckStatus(Item->child(i),NewDate);
	}
	//
}

void AuthorTreeModel::setParentUserCheckStatus(AuthorTreeList *Item, const QDate NewDate)
{
	//
	if(Item->parent()==nullptr)return;
	//
	QDate date=NewDate;
	//
	for(auto i=0;i<Item->childCount();++i){
		QDate tDate=Item->child(i)->getUserCheck();
		if(tDate<date)date=tDate;
	}
	//
	if(Item->getUserCheck()!=date){
		Item->setUserCheck(NewDate);
		//
		QModelIndex index=createIndex(Item->row(),0,Item);
		emit dataChanged(index,index);
		//
		setParentUserCheckStatus(Item->parent(),NewDate);
	}
	//
}

void AuthorTreeModel::setParentTextUpdateStatus(AuthorTreeList *Item,const QDate NewDate)
{
	//
	if(Item->parent()==nullptr)return;
	//
	QDate date=Item->getLastUpdate();
	//
	if(date<NewDate){
		Item->setLastUpdate(NewDate);
		//
		QModelIndex index=createIndex(Item->row(),0,Item);
		emit dataChanged(index,index);
		//
		setParentTextUpdateStatus(Item->parent(),NewDate);
	}
	//
}
