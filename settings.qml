import QtQuick 2.0
import QtQuick.Controls 2.11
//import QtQuick.Dialogs 1.3

Dialog  {
    id: settingDialog

    signal saveSettigs(bool toF,string log,string pass)

    //modality: Qt.ApplicationModal
    modal: true
    visible: true
    title: qsTr("Settings")

    width: Math.min(320,parent.width)
    height: Math.min(320,parent.height)

    x: (parent.width-width)/2
    y: (parent.height-height-header.height)/2

    property string txtLogin: ""
    property string txtPassword: ""
    property bool toFile: true

    Component.onCompleted: settingDialog.saveSettigs.connect(saveSettings)

//    header.visible: false

    contentItem: Rectangle {
        id: rectangle
        color: "#ffffff"
        border.color: color
        anchors.topMargin: header.height
        anchors.fill: parent

        ImageButton {
            id: ibtnCansel
            x: 328
            y: 214
            height: 72
            imagefillMode: Image.PreserveAspectFit
            anchors.left: parent.horizontalCenter
            anchors.leftMargin: 8
            anchors.right: parent.right
            anchors.rightMargin: 8
            anchors.top: textPassword.bottom
            anchors.topMargin: 8
            btnImageReleased: "qrc:/Image/icons8-delete-96.png"
            btnImagePressed: "qrc:/Image/icons8-delete-96.png"

            onBtnClicked: {
                settingDialog.close()
            }
        }

        ImageButton {
            id: ibtnOk
            x: 8
            y: 214
            height: 72
            imagefillMode: Image.PreserveAspectFit
            anchors.right: parent.horizontalCenter
            anchors.rightMargin: 8
            anchors.top: textPassword.bottom
            anchors.topMargin: 8
            anchors.left: parent.left
            anchors.leftMargin: 8
            btnImageReleased: "qrc:/Image/icons8-ok-96.png"
            btnImagePressed: "qrc:/Image/icons8-ok-96.png"
//            activeFocusOnTab: true

            onBtnClicked: {
                settingDialog.accept()
            }

        }

        RadioButton {
            id: rbtnServer
            x: 8
            y: 49
            height: 33
            text: qsTr("Save to server")
            checked: !toFile
            font.pointSize: 12
            anchors.left: parent.left
            anchors.topMargin: 8
            anchors.rightMargin: 8
            display: AbstractButton.TextBesideIcon
            anchors.top: rbtnFile.bottom
            anchors.leftMargin: 8
            anchors.right: parent.right
            wheelEnabled: false
        }

        RadioButton {
            id: rbtnFile
            x: 8
            y: 8
            height: 33
            text: qsTr("Save to file")
            font.pointSize: 12
            display: AbstractButton.TextBesideIcon
            wheelEnabled: false
            checked: toFile
            anchors.right: parent.right
            anchors.rightMargin: 8
            anchors.left: parent.left
            anchors.leftMargin: 8
            anchors.top: parent.top
            anchors.topMargin: 8
        }

        Label {
            id: labelPassword
            x: 8
            y: 152
            text: qsTr("Password")
            anchors.right: parent.right
            anchors.rightMargin: 8
            anchors.left: parent.left
            anchors.leftMargin: 8
            anchors.top: textLogin.bottom
            anchors.topMargin: 8
            font.pointSize: 12
        }

        TextField {
            id: textPassword
            x: 8
            y: 173
            text: txtPassword
            anchors.left: parent.left
            anchors.leftMargin: 8
            font.pointSize: 12
            anchors.topMargin: 2
            anchors.bottom: labelPassword.top
            anchors.rightMargin: 8
            anchors.top: labelPassword.bottom
            anchors.right: parent.right
            anchors.bottomMargin: -54
            echoMode: TextInput.PasswordEchoOnEdit
        }

        Label {
            id: label
            x: 8
            y: 90
            text: qsTr("Login")
            anchors.right: parent.right
            anchors.rightMargin: 8
            anchors.left: parent.left
            anchors.leftMargin: 8
            anchors.top: rbtnServer.bottom
            anchors.topMargin: 8
            font.pointSize: 12
        }

        TextField {
            id: textLogin
            x: 8
            y: 111
            text: txtLogin
            anchors.left: parent.left
            anchors.leftMargin: 8
            anchors.bottom: label.top
            anchors.bottomMargin: -54
            anchors.right: parent.right
            anchors.rightMargin: 8
            anchors.top: label.bottom
            anchors.topMargin: 2
            font.pointSize: 12
        }
    }

    footer.visible: false

    onAccepted:{
        saveSettigs(rbtnFile.checked,textLogin.text,textPassword.text)
    }
}












































































































