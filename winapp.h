#ifndef WINAPP_H
#define WINAPP_H

#include <QObject>
#include <QSharedPointer>

class QGuiApplication;
class QQmlApplicationEngine;
class QQuickWindow;

class SaveLoad;
class Interface;
class Resource;
class timeControl;


class WinApp : public QObject
{
	Q_OBJECT
public:
	WinApp(int argc, char *argv[], QObject *parent = nullptr);
	~WinApp();
	int exec();

private:
	QGuiApplication* app;
	QQmlApplicationEngine* engine;
	QQuickWindow* window;

	SaveLoad* File;
	Interface* Gui;
	Resource* Data;
	timeControl* Timer;

	int ErrorExit;
};

#endif // WINAPP_H
