#include "htmlsender.h"

#include <QNetworkReply>
#include <QTextCodec>

HTMLSender::HTMLSender(QObject *parent)
	: QObject(parent),NAManager{new QNetworkAccessManager(this)}
{
}

HTMLSender::~HTMLSender()
{
	NAManager->deleteLater();
}

void HTMLSender::sendData(QString Url, QVector<QPair<QString, QByteArray> > data)
{
	//
	QUrl url(Url);
	QNetworkRequest request(url);
	//
	QByteArray d;
	QString bound="APbound";
	//
	request.setRawHeader("Content-Type","multipart/form-data; boundary=APbound");
	d.append("--"+bound+"\r\n");
	for(auto p:data){
		d.append("Content-Disposition: form-data; name=\""+p.first+"\"\r\n\r\n");
		d.append(p.second);
		d.append("\r\n--" + bound +"\r\n");
	}
	request.setRawHeader("Content-Length", QString::number(d.length()).toUtf8());
	//
	QNetworkReply* reply=NAManager->post(request,d);
	QObject::connect(reply,&QNetworkReply::finished,this,&HTMLSender::slotHTMLFinish);
	//
}

void HTMLSender::slotHTMLFinish()
{
	//
	QNetworkReply* reply=qobject_cast<QNetworkReply *>(sender());
	if(reply->error()!=QNetworkReply::NoError){
		reply->deleteLater();
		emit postError();
		return;
	}
	QString st(reply->readAll());
	reply->deleteLater();
	//
	emit postSuccess(st);
	//
}


