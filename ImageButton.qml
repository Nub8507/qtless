import QtQuick 2.11

Item {

    signal btnClicked()

    property string btnImagePressed:""
    property string btnImageReleased:""
    property double btnOpacityPressed: 1
    property double btnOpacityReleased: 1
    property double imagefillMode: 0

    Image {
        id: btnImage
        scale: 1
        opacity: btnOpacityReleased
        source: btnImageReleased
        property bool pressed: false
        fillMode: imagefillMode

        anchors.fill: parent

        MouseArea {
            id: mouse_area8
            anchors.fill: parent

            onPressed:{
                if (btnImage.pressed == false) {
                    btnImage.state = "btnImage_PRESSED"
                    btnImage.pressed = true
                }
                else{
                    btnImage.state = "btnImage_off_RELEASED"
                    btnImage.pressed = false
                }
                btnClicked()
            }
        }

        states:[
            State {
                name: "btnImage_PRESSED"
                PropertyChanges { target: btnImage; source: btnImagePressed}
                PropertyChanges { target: btnImage; opacity: btnOpacityPressed;}
            },
            State {
                name: "btnImage_off_RELEASED"
                PropertyChanges { target: btnImage; source: btnImageReleased}
                PropertyChanges { target: btnImage; opacity: btnOpacityReleased; }
            }
        ]

        Behavior on opacity
        {NumberAnimation { duration: 100 }}
    }
}



















/*##^## Designer {
    D{i:0;autoSize:true;height:480;width:640}D{i:1;anchors_height:100;anchors_width:100;anchors_x:0;anchors_y:0}
}
 ##^##*/
