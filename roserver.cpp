#include "roserver.h"

#include <QAbstractItemModel>

ROServer::ROServer(QObject *parent):SLViewQtRemouteSimpleSource(parent)
{
}

ROServer::~ROServer()
{
}

void ROServer::r_startParce(QString txtHTML)
{
	//
	emit startParce(txtHTML);
	//
}

void ROServer::r_AddAuthor(QString Author)
{
	//
	emit AddAuthor(Author);
	//
}

void ROServer::r_UpdateAll()
{
	//
	emit updateAll();
	//
}

void ROServer::r_SaveAll()
{
	//
	emit SaveAll();
	//
}

void ROServer::r_userCheckedUpdate(QString index)
{
	//
	ContentElement elem;
	elem.init(index);
	emit userCheckedUpdate(elem);
	//
}

void ROServer::r_del(QString index)
{
	//
	ContentElement elem;
	elem.init(index);
	emit del(elem);
	//
}

void ROServer::r_getAllContentData()
{
	emit AllContentData();
}

void ROServer::initUserData(QList<QString> Authors, QList<AuthorTreeList::NodeItem> elements)
{
	//
	QList<QString> list{};
	//
	for(auto p:elements){
		list.push_back(p.toString());
	}
	//
	emit r_initUserData(Authors,list);
	//
}

void ROServer::ElementChanged(AuthorTreeList::NodeItem elem)
{
	//
	emit r_ElementChanged(elem.toString());
	//
}

void ROServer::ElementDeleted(AuthorTreeList::NodeItem elem)
{
	//
	emit r_ElementDeleted(elem.toString());
	//
}
