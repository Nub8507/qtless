#ifndef SAVELOAD_H
#define SAVELOAD_H

#include "contentelement.h"


#include <QObject>

class SaveLoad: public QObject
{
	Q_OBJECT
public:
	explicit SaveLoad(QObject *parent = nullptr)
		:QObject(parent)
	{
	}
	//

	enum Direction{
		FILE,
		SERVER
	};

	QList<QString> LoadAuthors();
	QVector<ContentElement> LoadSavedList();

signals:

public slots:
    void SaveAuthor(const QVector<QString>& Authors);
    void SaveTextsList(const QVector<ContentElement> &list);

private:
	QList<QString> LoadAuthorsFromFile(QString file="Authors.txt", bool AppendPath=true);
    QVector<ContentElement> LoadSavedListFromFile(QString file="Texts.txt", bool AppendPath=true);
	//
    void SaveAuthorToFile(const QVector<QString>& Authors,QString file="Authors.txt", bool AppendPath=true);
    void SaveTextsListToFile(const QVector<ContentElement> &list,QString file="Texts.txt", bool AppendPath=true);
	//
};

#endif // SAVELOAD_H
