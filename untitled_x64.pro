#-------------------------------------------------
#
# Project created by QtCreator 2018-10-14T22:22:20
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets webenginewidgets network quick quickwidgets

TARGET = untitled
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += C++11 C++14

SOURCES += \
        main_64.cpp \
    namevalidator.cpp \
    widget.cpp \
    webbrowser.cpp\
    webview.cpp \
    staticfunctions.cpp \
    regexttest.cpp \
    parcerslauthor.cpp \
    interface.cpp \
    authortreelist.cpp \
    authortreemodel.cpp \
    qmlparce.cpp

HEADERS += \
        widget.h \
    namevalidator.h \
    widget.h \
    webbrowser.h\
    webview.h \
    staticfunctions.h \
    regexttest.h \
    contentelement.h \
    parcerslauthor.h \
    interface.h \
    authortreelist.h \
    authortreemodel.h \
    qmlparce.h

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES +=

RESOURCES += \
    qml.qrc
