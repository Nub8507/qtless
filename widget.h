#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>

class QDateTimeEdit;
class WebView;

class Widget : public QWidget
{
    Q_OBJECT
private:

    QDateTimeEdit* pDateTimeEdit;
    WebView* pWebV=nullptr;

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();

    void showHTML(QString url);

public slots:
    void btnClick();
};

#endif // WIDGET_H
