#ifndef SETTINGS_H
#define SETTINGS_H

#include <QCryptographicHash>
#include <QSettings>
#include <QStandardPaths>
#include <QString>
#include "saveload.h"

namespace Settings {

    static QString getFilePath(){
		auto path = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
		auto fileName= path + "/"+"settings.conf";
		return fileName;
	}

	static SaveLoad::Direction getSaveDir(){
		SaveLoad::Direction SaveDir;
		QSettings set(getFilePath(), QSettings::IniFormat);
		int sd=set.value("SaveDir",0).toInt();
		switch (sd) {
		case 0:
			SaveDir=SaveLoad::FILE;
			break;
		case 1:
			SaveDir=SaveLoad::SERVER;
			break;
		default:
			SaveDir=SaveLoad::FILE;
		}
		return SaveDir;
	}
	inline void setSaveDir(SaveLoad::Direction Dir){
		QSettings set(getFilePath(), QSettings::IniFormat);
		int sd=Dir;
		set.setValue("SaveDir",sd);
	}

	inline QString getLogin(){
		QSettings set(getFilePath(), QSettings::IniFormat);
		return set.value("Login","").toString();
	}
	inline void setLogin(const QString &value){
		QSettings set(getFilePath(), QSettings::IniFormat);
		set.setValue("Login",value);
	}

	inline QString getPassword() {
		QSettings set(getFilePath(), QSettings::IniFormat);
		return set.value("Password","").toString();
	}
	inline void setPassword(const QString &value){
		QSettings set(getFilePath(), QSettings::IniFormat);
		set.setValue("Password",value);
	}

	static QString getPassHash(){
		//
		QSettings set(getFilePath(), QSettings::IniFormat);
		QString Pass=set.value("Password","").toString();
		QCryptographicHash hash(QCryptographicHash::Md5);
		hash.addData(Pass.toUtf8());
		QString res(hash.result());
		return res;
		//
	}

}


#endif // SETTINGS_H
