#include "roreplica.h"

#include <QRemoteObjectReplica>

RoReplica::RoReplica(QSharedPointer<QRemoteObjectDynamicReplica> ptr,QObject* parent):
	QObject(parent), reptr(ptr)
{
	//
	QObject::connect(reptr.data(), SIGNAL(initialized()), this, SLOT(initConnection_slot()));
//	QObject::connect(reptr.data(), SIGNAL(initialized()), reptr.data(), SLOT(r_getAllContentData()));
	QObject::connect(reptr.data(), &QRemoteObjectReplica::stateChanged, this, &RoReplica::ConnectionStatus_slot);
	//
}

RoReplica::~RoReplica()
{
	//

	//
}

bool RoReplica::isReplicaValid()
{
	return reptr->isReplicaValid();
}

void RoReplica::initConnection_slot()
{
	//
	QObject::connect(reptr.data(), SIGNAL(r_sendTrayMessage(const QString,const QList<QString>)), this, SIGNAL(sendTrayMessage(const QString,const QList<QString>)));
	QObject::connect(reptr.data(), SIGNAL(r_initUserData(QList<QString>,QList<QString>)), this, SLOT(r_initUserData(QList<QString>,QList<QString>)));
	QObject::connect(reptr.data(), SIGNAL(r_ElementChanged(QString)), this, SLOT(r_ElementChanged(QString)));
	QObject::connect(reptr.data(), SIGNAL(r_ElementDeleted(QString)), this, SLOT(r_ElementDeleted(QString)));
	//
	QObject::connect(this, SIGNAL(r_startParce(QString)), reptr.data(), SLOT(r_startParce(QString)));
	QObject::connect(this, SIGNAL(r_AddAuthor(QString)), reptr.data(), SLOT(r_AddAuthor(QString)));
	QObject::connect(this, SIGNAL(r_UpdateAll()), reptr.data(), SLOT(r_UpdateAll()));
	QObject::connect(this, SIGNAL(r_SaveAll()), reptr.data(), SLOT(r_SaveAll()));
	QObject::connect(this, SIGNAL(r_userCheckedUpdate(QString)), reptr.data(), SLOT(r_userCheckedUpdate(QString)));
	QObject::connect(this, SIGNAL(r_del(QString)), reptr.data(), SLOT(r_del(QString)));
	//
	QObject::connect(this, SIGNAL(initialized()), reptr.data(), SLOT(r_getAllContentData()));
	//
	emit initialized();
	//
}

void RoReplica::ConnectionStatus_slot(QRemoteObjectReplica::State state, QRemoteObjectReplica::State oldState)
{
	//
	if(state==QRemoteObjectReplica::Uninitialized||
			state==QRemoteObjectReplica::Suspect||
			state==QRemoteObjectReplica::SignatureMismatch)
		emit connectionLost();
	//
}

void RoReplica::startParce(QString txtHTML)
{
	//
	emit r_startParce(txtHTML);
	//
}

void RoReplica::AddAuthor(QString Author)
{
	emit r_AddAuthor(Author);
}

void RoReplica::UpdateAll()
{
	emit r_UpdateAll();
}

void RoReplica::SaveAll()
{
	emit r_SaveAll();
}

void RoReplica::userCheckedUpdate(QModelIndex index)
{
	auto Item = static_cast<AuthorTreeList*>(index.internalPointer());
	emit r_userCheckedUpdate(Item->data().source.toString());
}

void RoReplica::del(QModelIndex index)
{
	auto Item = static_cast<AuthorTreeList*>(index.internalPointer());
	emit r_del(Item->data().source.toString());
}

void RoReplica::r_sendTrayMessage(const QString title, const QList<QString> txtList)
{
	emit sendTrayMessage(title,txtList);
}

void RoReplica::r_initUserData(QList<QString> Authors, QList<QString> dataList)
{
	//
    QVector<ContentElement> list{};
    ContentElement t;
	for(auto p:dataList){
        t.init(p);
        list.append(t);
	}
    //
    emit initUserData(Authors,list);
	//
}

void RoReplica::r_ElementChanged(QString strElem)
{
    ContentElement t;
    t.init(strElem);
	emit ElementChanged(t);
}

void RoReplica::r_ElementDeleted(QString strElem)
{
    ContentElement t;
    t.init(strElem);
	emit ElementDeleted(t);
}
