#include "parcerslauthor.h"
#include "resource.h"

#include <QDesktopServices>
#include <QThread>

Resource::Resource(QObject *parent) : QObject(parent)
{
	QObject::connect(this,&Resource::ParceFinish,this,&Resource::changeList);	//соединение для обработкирасшифровщика страниц
//	QObject::connect(&TLModel,&AuthorTreeModel::dataChanged,this,&Resource::dataChanged);
}

AuthorTreeModel *Resource::getTLModel()
{
	return &TLModel;
}

QList<QString> Resource::getAuthors() const
{
	return Authors;
}

//заполнить спиисок авторов
void Resource::setAuthors(const QList<QString> &value)
{
	Authors.clear();
	//
	for(auto p:value)
		addAuthor(p);
}

//добавить автора
void Resource::addAuthor(QString Author)
{
	//
	QString t=Author.trimmed();
	//
	if (!Authors.contains(t))Authors.append(t);
	//
	emit FinishAuthorChange();
	//
}

//начальное заполнение дерева из списка
void Resource::initList(QVector<ContentElement> &list)
{
	//
	TLModel.clear();
	//
	changeList(list);
	//
	emit ModelUpdated();
	//
}

//нужно сравнить и обновить дерево произведений
void Resource::changeList(QVector<ContentElement> &list)
{
	compare(list);
}

//нужно сохранить спиcок произведений
void Resource::saveList()
{
	//
	emit DataListReadyToSave(treeToList(TreeList.FindRoot()));
	//
}


//нужно сохранить список авторов и произведений
void Resource::saveAll()
{
	//
	saveAuthors();
	saveList();
	//
}

//нужно сохранить список авторов
void Resource::saveAuthors()
{
	QVector<QString> res;
	for(auto p:Authors)
		res.push_back(p);
	emit AuthorsListReadyToSave(res);
}

//пользователь посмотрел произведение, нужно пометить о просмотре
void Resource::userCheckedUpdate(ContentElement elem)
{
	//
	AuthorTreeList* index=TreeList.Find(elem);
	TLModel.changeGroupUserCheckStatus(index,QDate::currentDate());
	//
	emit ModelUpdated();
	//
}

void Resource::startParce(QString txtHTML)
{
	//
	ParcerSL::ParcerSLAuthor* t=new ParcerSL::ParcerSLAuthor();
	t->setHTML(txtHTML.trimmed());
	QThread* tread=new QThread();
	connect(tread,&QThread::started,t,&ParcerSL::ParcerSLAuthor::Start);
	connect(tread, &QThread::finished, tread, &QThread::deleteLater);
	connect(t,&ParcerSL::ParcerSLAuthor::finished,this,&Resource::slotParceFinished);
	t->moveToThread(tread);
	tread->start();
	//
}

//пользователь удалил автора
void Resource::userDelAuthor(ContentElement elem)
{
	AuthorTreeList* index=TreeList.Find(elem);
	//QString Author=index->getHTMLAdress();
	QString Author=elem.HTMLAuthorAdress;
	bool result=TLModel.removeRows(TLModel.findIndexData(index).row(),1,TLModel.findIndexData(index).parent());
	if (result){
		deleteAuthor(Author);
		emit ElementDeleted(elem);
	}
	//
	emit ModelUpdated();
	emit FinishAuthorChange();
	//
}

//проверяет и очищает дерево на наличие удалннных элементов
void Resource::checkDeleted()
{
	//
	AuthorTreeList* t=TreeList.FindRoot();
	//
	checkDeleted(t);
	//
	emit ModelUpdated();
	//
}

//устанавливает пометку удаления всем произведениям автора
void Resource::setDeleted(QString Author)
{
	//
	AuthorTreeList* t=TreeList.FindRoot();
	//
	for (int i=0;i<t->childCount();++i) {
		if(t->child(i)->getHTMLAdress()==Author)
			setDeleted(t->child(i));
	}
	//
}

//запрос всех данных
void Resource::getAllContentData()
{
	//
	auto list=this->treeToList(&TreeList);
	//
	emit AllContentData(getAuthors(),list);
	//
}

//изменился элемент
void Resource::elementChange(ContentElement elem)
{
	//
//	AuthorTreeList* t=TreeList.Find(elem);
//	if(t==nullptr){
//		compare({elem});
//	}
//	else {
//		t->data().fromContElement(elem);
//		TLModel.changeGroupUserCheckStatus(t,t->getUserCheck());
//	}
    QVector<ContentElement> t{elem};
    compare(t);
	//
}

//удалили элемент
void Resource::elementDelete(ContentElement elem)
{
	//
	AuthorTreeList* t=TreeList.Find(elem);
	if(t==nullptr)return;
	t->setDeleted(true);
	checkDeleted(t);
	//
}

//заполнение списка авторов и произведений
void Resource::initAllData(QList<QString> Authors, QVector<ContentElement> list)
{
	//
	setAuthors(Authors);
	//
	initList(list);
	//
	emit FinishDataLoading();
	//
}

//вызывается при окончании расшифровки HTML-страницы
void Resource::slotParceFinished()
{
	//
    QVector<ContentElement>t{};
	//
	ParcerSL::ParcerSLAuthor* reply=qobject_cast<ParcerSL::ParcerSLAuthor *>(sender());
	if(reply==nullptr)return;
	if(reply->parceStatus()){
        t.append(reply->getResult());
		emit addAuthor(reply->getHTML());
	}
	//
	std::sort(t.begin(),t.end());
	QThread *tread;
	tread=reply->thread();
	reply->deleteLater();
	tread->quit();
	//
	if(!t.isEmpty())
		setDeleted(t[0].HTMLAuthorAdress);
	//
	emit ParceFinish(t);
	//
}

//удаление автора
void Resource::deleteAuthor(QString Author)
{
	//
	this->Authors.removeAll(Author);
	//
}

//вызывается при изменении модели
void Resource::dataChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight, const QVector<int> &roles)
{
	//

//	if(topLeft==bottomRight){
//		auto t=static_cast<AuthorTreeList*>(topLeft.internalPointer());
//		emit ElementChanged(t->data().source);
//	}
	//
}

//функция сравения и изменения произведений в дереве
void Resource::compare(QVector<ContentElement> &list)
{
	//
	AuthorTreeList* T;
	QVector<QString> txtNotify;
    //
    std::sort(list.begin(),list.end());
    //
	for(auto p:list){
		T=TreeList.Find(p);								//пробуем найти произведение в дереве
		if(T==nullptr){									//если не найдено
			T=TLModel.add(p);							//добавляем в дерево
			TLModel.setTextUpdated(T,p.LastUpdate);		//обновить дерево
			T->setDeleted(false);
			emit ElementChanged(p);
			TLModel.changeGroupUserCheckStatus(T,T->getUserCheck());
            if(p.HTMLContentAdress!="")
				txtNotify.push_back(fillTextNotify(p));	//добавим строку оповещения
		}
		else {											//если произведение найдено
			#ifdef  QT_DEBUG
				auto d1=T->data().getLastUpdate();
				auto d2=p.LastUpdate;
				auto d3=T->data();
			#endif										//проверяем дату обновления
			if(T->data().getLastUpdate()<p.LastUpdate){	//если дата обновления поменялась
				TLModel.setTextUpdated(T,p.LastUpdate);	//обновить дерево
				emit ElementChanged(p);
				TLModel.changeGroupUserCheckStatus(T,T->getUserCheck());
                if(p.HTMLContentAdress!="")
					txtNotify.push_back(fillTextNotify(p));	//добавим строку оповещения
			}
			T->setDeleted(false);
		}
	}
	//
	if(txtNotify.count()>0)										//если произошли обновления
		emit SendTrayMessage("Обновились произведения",txtNotify);	//сообщить об этом пользователю
	//
	checkDeleted();
	//
}

//сохраняет дерево произведений в список
QVector<ContentElement> Resource::treeToList(AuthorTreeList *parent)
{
	//
	QVector<ContentElement> list{};
	//
	if(parent->childCount()==0)return list;
	//
	for(auto p:parent->children()){
		list<<p->data().source;
		list<<treeToList(p);
	}
	//
	return list;
	//
}

//заполняет сообщение пользователю
QString Resource::fillTextNotify(const ContentElement &elem)
{
	//
	return elem.Author.trimmed()+": "+elem.Content.trimmed();
	//
}

//проверяет и удаляет дочерние элементы дерева, помеченные на удаление
void Resource::checkDeleted(AuthorTreeList* index)
{
	//
	for(auto p:index->children()){
		checkDeleted(p);
	}
	//
	if(index->childCount()==0){
		if(index->getDeleted()){
#ifdef QT_DEBUG
			int t1=index->row();
			auto t2=TLModel.findIndexData(index->parent());
			auto t3=static_cast<AuthorTreeList*>(t2.internalPointer());
#endif
			const ContentElement& t=index->data().source;
			bool result=TLModel.removeRows(index->row(),1,TLModel.findIndexData(index->parent()));
			if(result)
				emit ElementDeleted(t);
			return;
		}
	}
	//
	index->setDeleted(false);
	//
}

//помечает элемент и дочерние элементы дерева на удаление
void Resource::setDeleted(AuthorTreeList* index)
{
	//
	if(index->parent()!=index->FindRoot())
		index->setDeleted(true);
	//
	for (int i=0;i<index->childCount();++i) {
		setDeleted(index->child(i));
	}
	//
}

