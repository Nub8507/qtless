#ifndef AUTHORTREELIST_H
#define AUTHORTREELIST_H

#include <contentelement.h>

//класс для хранения дерева текстов
class AuthorTreeList
{
    //
public:

	//данные об элементе
    struct NodeItem
    {
		QString Group;							//строка заголовка
		QString HTMLAdress;						//HTML адрес элемента
		QString Anotation;						//описание элемента
		bool deleted;							//флаг удаления произведения

		ContentElement source;					//исходный элемент списка

        NodeItem(){
            Group="";
            HTMLAdress="";
            Anotation="";
            deleted=false;
        }

		//заполнение элемента по исходным данным
		void fromContElement(const ContentElement &data)
		{
            Anotation=data.Anotation;
            source=data;
            deleted=false;
            //
            if(data.HTMLContentAdress!=""){
                Group=data.Content;
                HTMLAdress=data.HTMLAuthorAdress+data.HTMLContentAdress;
            }else if(data.HTMLGroupAdress!=""||data.Group!=""){
                Group=data.Group;
                int GroupType;
                GroupType=data.HTMLGroupAdress.indexOf("/type/",0,Qt::CaseInsensitive);
                if(GroupType==-1)
                    HTMLAdress=data.HTMLAuthorAdress+data.HTMLGroupAdress;
                else
                    HTMLAdress=data.HTMLAuthorAdress;
            }else if(data.HTMLAuthorAdress!=""){
                Group=data.Author;
                HTMLAdress=data.HTMLAuthorAdress;
            }else{
                Group=data.Author;
            }
			//
		}

		friend QTextStream& operator<<(QTextStream& out,const NodeItem& elem)
		{
			QTextCodec* tt=out.codec();
			out.setCodec("UTF-8");
			//
			out<<elem.source;
			//
			out.setCodec(tt);
			return out;
		}

		friend QTextStream& operator>>(QTextStream& in,NodeItem& elem)
		{
			QTextCodec* tt=in.codec();
			in.setCodec("UTF-8");
			//
			ContentElement el;
			//
			if(!in.atEnd())
				in>>el;
			//
			elem.fromContElement(el);
			//
			in.setCodec(tt);
			return in;
		}

		QString toString(){
			//
//			QString S="";
//			//
//			QTextStream in(&S);
//			in<<*this;
//			//
//			return S;
			return this->source.toString();
			//
		}

		void fromString(QString str){
//			QTextStream out(&str);
//			out>>*this;
			ContentElement el;
			el.init(str);
			this->fromContElement(el);
		}

		QDate getUserCheck() const{return source.UserCheck;}
		void setUserCheck(const QDate &value){source.UserCheck=value;}

		QDate getLastUpdate() const{return source.LastUpdate;}
		void setLastUpdate(const QDate &value){source.LastUpdate=value;}

	private:
		const QString readLine(QTextStream& in){
			//
			QString result="",t="";
			//
			while(!in.atEnd()&&t!="}} ")
			{
				result+=t;
				in>>t;
				t+=" ";
			}
			//
			return result.trimmed();
		}

	};

	AuthorTreeList();
	AuthorTreeList(AuthorTreeList *parent);
	AuthorTreeList(const ContentElement& Element,AuthorTreeList *parent);

    ~AuthorTreeList();

	//добавляем дочерний элемент к дереву
    AuthorTreeList* appendChild(const ContentElement& Element);

	//получаем дочерний элемент по индексу
    AuthorTreeList* child(int row);

	QList<AuthorTreeList*>& children(){return m_childItems;}

	//количество дочерних элементов
    int childCount()const;

	//количество колонок для модели treeview
	int columnCount()const;

	//номер элемента этого уровня группировки
    int row() const;

	//получение данных дерева
	NodeItem &data();

	//получение родительского элемента
    AuthorTreeList* parent();

	//заполнение дерева данными из отсортированного списка, список должен содержать все уровни дерева	
	template <typename T>
	void init(T first,T end,int deep=1);

    int ElemCount();

	//поиск элемента в дереве
    AuthorTreeList* Find(const ContentElement &data);

	//поиск родителя элемента в дереве
    AuthorTreeList* FindParent(const ContentElement &data);

	//добавление элемента в дерево, поиск родителя осуществляется автоматически.
	AuthorTreeList* add(const ContentElement &data);

	AuthorTreeList* add(const NodeItem &data);

	//удаление элемента дерева
	bool del(AuthorTreeList* data);

	//Очищает дерево
	AuthorTreeList* clear();

	//Находит номер дочернего элемента
	int findRow(AuthorTreeList*);

	//поиск корневого элемента
	AuthorTreeList* FindRoot();

	QDate getUserCheck() const;
	void setUserCheck(const QDate &value);

	QDate getLastUpdate() const;
	void setLastUpdate(const QDate &value);

	QString getHTMLAdress() const;

	bool getDeleted() const;
	void setDeleted(bool value);


private:

	AuthorTreeList* m_parentItem;						//ссылка на родителя
	QList<AuthorTreeList*> m_childItems;				//список дочерних элементов
	NodeItem m_itemData;								//данные

	//поиск элемента по конткретной ветви дерева
    AuthorTreeList* FindFromRoot(const ContentElement &data,AuthorTreeList* root,int deep=3);
    //
};

#endif // AUTHORTREELIST_H
