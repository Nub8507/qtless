#include "timecontrol.h"

timeControl::timeControl(QObject *parent) : QObject(parent)
{
	timerListUpdate.stop();						//при создании класса останавливаем таймеры
	timerSaveData.stop();
#ifdef QT_DEBUG
	timerDebugMessage.stop();
//	timerDebugMessage.start(1000*20);
#endif
	//
	needAllUpdate=false;
	//
	QObject::connect(&timerListUpdate,&QTimer::timeout,this,&timeControl::needUpdateAuthor);
	QObject::connect(&timerSaveData,&QTimer::timeout,this,&timeControl::needSaveData);
#ifdef QT_DEBUG
	QObject::connect(&timerDebugMessage,&QTimer::timeout,this,&timeControl::needDebugMessageSend);
#endif
	//
}

//устанавливаем интервалы работы таймеров
void timeControl::startWork(int interval)
{
	timerListUpdate.start(interval);
	timerSaveData.start(interval*5);
#ifdef QT_DEBUG
//	timerDebugMessage.start(1000*20);
#endif
}

//останавливаем таймеры
void timeControl::stopWork()
{
	timerListUpdate.stop();
	timerSaveData.stop();
#ifdef QT_DEBUG
	timerDebugMessage.stop();
#endif

}

//вызывается для обновления всех авторов
void timeControl::updateAll()
{
	if(Res->getAuthors().length()==0)return;	//если список авторов пуст ничего не делаем
	lastAuhor=Res->getAuthors()[Res->getAuthors().length()-1];	//выбираем последнего автора в списке
																//это нужно для правильного срабатывания needUpdateAuthor()
	needAllUpdate=true;							//устанавливаем флаг обновления всех
	emit needUpdateAuthor();					//запускаем принудительно обновление
}

//вызывается после расшифровки данных автора
//для проверки необходимости обновления следующего автора
void timeControl::parceFinish()
{
	//
	if(needAllUpdate)				//если стоит флаг обновления всех
		emit needUpdateAuthor();	//сразу вызываем обновление следующего автора
	//
}

//вызывается для обработки следующего автора
void timeControl::needUpdateAuthor()
{
	if(Res->getAuthors().length()==0)return;			//если список авторов пуст ничего не делаем
	//
	if(lastAuhor.isEmpty()){							//если последний обновленный автор не заполнен
		lastAuhor=Res->getAuthors()[0];					//выбираем первого в списке
		emit StartUpdate(lastAuhor);					//и начинаем обновление
		return;
	}
	//
	int index=Res->getAuthors().indexOf(lastAuhor);		//получаем индекс автора в списке
	if(index==Res->getAuthors().length()-1||index==-1)	//если это последний автор или автор не найден
		index=0;										//получаем индекс первого автора
	else												//иначе
		++index;										//индекс следующего
	//
	lastAuhor=Res->getAuthors()[index];					//получаем автора по индексу
	emit StartUpdate(lastAuhor);						//вызываем обновление автора
	//
	if(index==Res->getAuthors().length()-1)				//если это последний автор в списке
		needAllUpdate=false;							//сбрасываем флаг обновления всех
	//
}

//срабатывение таймера сохранения
void timeControl::needSaveData()
{
	emit SaveList();									//вызывает требование сохранить данные
}

#ifdef QT_DEBUG
//срабатывение таймера отладочного сообщения
void timeControl::needDebugMessageSend()
{
	emit DebugMessage("Время",{QDateTime::currentDateTime().toString("dd.MM.yyyy HH:mm:ss")});
}
#endif

//получаем ссылку на данные
void timeControl::setResource(Resource *value)
{
	Res=value;
}

