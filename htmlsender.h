#ifndef HTMLSENDER_H
#define HTMLSENDER_H

#include <QNetworkAccessManager>
#include <QObject>

class HTMLSender : public QObject
{
	Q_OBJECT
public:
	explicit HTMLSender(QObject *parent = nullptr);
	~HTMLSender();

	void sendData(QString Url, QVector<QPair<QString,QByteArray>> data);

private:

	QNetworkAccessManager* NAManager;

signals:
	void postError();
	void postSuccess(QString data);

public slots:
	void slotHTMLFinish();
};

#endif // HTMLSENDER_H
