#include "interface.h"
#include "resource.h"
#include "saveload.h"
#include "settings.h"
#include "timecontrol.h"
#include "winapp.h"

#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQuickWindow>

WinApp::WinApp(int argc, char *argv[], QObject *parent) : QObject(parent)
{
	QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
//	QCoreApplication::setAttribute(Qt::AA_UseSoftwareOpenGL);

	app=new QGuiApplication(argc, argv);
	engine=new QQmlApplicationEngine (app);
	//
	File=new SaveLoad(app);
	Gui= new Interface(app);
	Data= new Resource(app);
	Timer= new timeControl(app);

	Data->setAuthors(File->LoadAuthors());

    {
        auto bookList=File->LoadSavedList();
        Data->initList(bookList);
    }

	Timer->setResource(Data);

#ifdef QT_DEBUG
	Timer->startWork(3*60*1000);//3 минуты
#else
	Timer->startWork(3*60*60*1000);//3 часа
#endif

	ErrorExit=0;
	engine->load(QUrl("qrc:/main.qml"));
	if (engine->rootObjects().isEmpty()){
		ErrorExit=-1;
		return;
	}
	//
	window=dynamic_cast<QQuickWindow*>(engine->rootObjects()[0]);
	window->setProperty("tmodel",QVariant::fromValue(Data->getTLModel()));

	QObject::connect(window,SIGNAL(startParce(QString)),Gui,SLOT(startParce(QString)));
	QObject::connect(window,SIGNAL(updateChecked(QVariant)),Gui,SLOT(updateChecked(QVariant)));
	QObject::connect(window,SIGNAL(updateAll()),Gui,SLOT(updateAll()));
	QObject::connect(window,SIGNAL(del(QVariant)),Gui,SLOT(del(QVariant)));
	QObject::connect(window,SIGNAL(needLoadSettings()),Gui,SLOT(needLoadSettings()));
	QObject::connect(window,SIGNAL(saveSettings(QVariant,QVariant,QVariant)),Gui,SLOT(saveSettings(QVariant,QVariant,QVariant)));
	//
//	QObject::connect(Data,SIGNAL(ModelUpdated()),window,SLOT(setModelUpdate()));
    QObject::connect(Data,SIGNAL(DataListReadyToSave(const QVector<ContentElement>& )),File,SLOT(SaveTextsList(const QVector<ContentElement>&)));
    QObject::connect(Data,SIGNAL(AuthorsListReadyToSave(const QVector<QString>& )),File,SLOT(SaveAuthor(const QVector<QString>&)));
    QObject::connect(Data,&Resource::SendTrayMessage,Gui,&Interface::sendTrayMessage);
	QObject::connect(Data,&Resource::ParceFinish,Timer,&timeControl::parceFinish);
	QObject::connect(Data,&Resource::ModelUpdated,Data,&Resource::saveList);
	QObject::connect(Data,&Resource::FinishAuthorChange,Data,&Resource::saveAuthors);
	//
	QObject::connect(Gui,&Interface::StartParce,Data,&Resource::startParce);
//	QObject::connect(Gui,SIGNAL(AddAuthor(QString)),Data,SLOT(AddAuthor(QString)));
	QObject::connect(Gui,SIGNAL(SendTrayMessage(QVariant,QVariant)),window,SLOT(setSysTrayMessge(QVariant,QVariant)));
	QObject::connect(Gui,&Interface::UserCheckedUpdate,Data,&Resource::userCheckedUpdate);
    QObject::connect(Gui,&Interface::Del,Data,&Resource::userDelAuthor);
	QObject::connect(Gui,&Interface::UpdateAll,Timer,&timeControl::updateAll);
	QObject::connect(Gui,SIGNAL(LoadSettings(QVariant,QVariant,QVariant)),window,SLOT(loadSettings(QVariant,QVariant,QVariant)));
	//
	QObject::connect(Timer,&timeControl::StartUpdate,Data,&Resource::startParce);
	QObject::connect(Timer,&timeControl::SaveList,Data,&Resource::saveList);
	//
	QObject::connect(app,&QGuiApplication::aboutToQuit,Data,&Resource::saveAll);
	//
	QObject::connect(engine,&QQmlApplicationEngine::quit,app,&QGuiApplication::quit);
	//
	app->setQuitOnLastWindowClosed(false);
	//
}

WinApp::~WinApp()
{
	app->deleteLater();
}

int WinApp::exec()
{
	//
	if(ErrorExit!=0){
		app->exit(ErrorExit);
		return ErrorExit;
	}
	//
	return app->exec();
	//
}
