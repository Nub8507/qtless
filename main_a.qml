import QtQuick 2.11
import QtQuick.Window 2.11
import QtQuick.Controls 1.4 as Control1_4
import QtQuick.Controls.Styles 1.4
import QtQuick.Controls 2.11 as Control2_11
import Qt.labs.platform 1.1

Control2_11.ApplicationWindow  {
    id: mainWin
    visible: true
    width: 320
    height: 240
    title: qsTr("SLViewer")

//    property bool tvSetVisible: false;
    property variant tmodel;

    signal startParce(string txt)
    signal updateChecked(var index)
    signal updateAll()
    signal del(var index)

    function setTreeModel(treeModel){
        //tmodel=treeModel
        //treeView.visible=true
    }

    function setModelUpdate(messaga){
        dialogDebug.mess=messaga
        dialogDebug.open()
    }

    function setSysTrayMessge(Title,Text){
        //treeView.update()
        sysTray.showMessage(Title,Text);
    }

    Rectangle {
        id: rectangle
        color: "#ffffff"
        anchors.rightMargin: 0
        anchors.bottomMargin: 0
        anchors.leftMargin: 0
        anchors.topMargin: 0
        anchors.fill: parent

        ImageButton {
            id: btnAdd
            x: 557
            width: 30
            height: 30
            visible: true
            //            text: qsTr("Add")
            anchors.top: parent.top
            anchors.topMargin: 3
            anchors.right: btnUpdateAll.left
            anchors.rightMargin: 10
            //            visible: !tvSetVisible
            btnImagePressed: "qrc:/Image/icons8-add-96.png"
            btnImageReleased: "qrc:/Image/icons8-add-96.png"

            onBtnClicked: {
                startParce(textEdit.text)
                textEdit.text=""
            }
        }

        ImageButton {
            id: btnUpdateAll
            x: 263
            width: 30
            height: 30
            //            text: qsTr("Update")
            //            rightPadding: 6
//            leftPadding: 6
//            bottomPadding: 4
//            topPadding: 4
            anchors.right: btnSettings.left
            anchors.rightMargin: 8
            anchors.top: parent.top
            anchors.topMargin: 3
            btnImagePressed: "qrc:/Image/icons8-refresh-96.png"
            btnImageReleased: "qrc:/Image/icons8-refresh-96.png"
            onBtnClicked: {
                updateAll()
            }
        }

        ImageButton {
            id: btnSettings
            x: 263
            width: 30
            height: 30
            anchors.right: parent.right
            anchors.rightMargin: 10
            anchors.top: parent.top
            anchors.topMargin: 3
            btnImagePressed: "qrc:/Image/icons8-settings-96.png"
            btnImageReleased: "qrc:/Image/icons8-settings-96.png"
            onBtnClicked: {
                var comp = Qt.createComponent("qrc:/settings.qml");
                if( comp.status != Component.Ready )
                {
                    if( comp.status == Component.Error )
                        console.debug("Error:"+ comp.errorString() );
                    return; // or maybe throw
                }
                var obj1 = comp.createObject(rectangle);
                obj1.open();
            }
        }

        TextEdit {
            id: textEdit
            text: ""
            clip: true
            textFormat: Text.AutoText
            anchors.rightMargin: 6
            anchors.leftMargin: 8
            anchors.bottom: parent.top
            anchors.right: btnAdd.left
            anchors.left: parent.left
            anchors.top: parent.top
            anchors.bottomMargin: -31
            anchors.topMargin: 8
            font.pixelSize: 16
            selectByMouse: true
        }


        Control1_4.TreeView {
            id: treeView
            verticalScrollBarPolicy: Qt.ScrollBarAlwaysOn
            horizontalScrollBarPolicy: Qt.ScrollBarAlwaysOff
            headerVisible: false
            visible: true

            style: TreeViewStyle  {
                   branchDelegate: Rectangle {visible: false}
            }

            anchors.rightMargin: 2
            anchors.bottomMargin: 2
            anchors.leftMargin: 2
            anchors.top: textEdit.bottom
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            anchors.left: parent.left
            anchors.topMargin: 6

            model: tmodel

            Control1_4.TableViewColumn {
//                title: "Name"
//                role: "fileName"
                width: treeView.width-25
            }
            rowDelegate: Rectangle {
                height: 150
                anchors.right: parent.right
                anchors.left: parent.left
            }

            itemDelegate: Item {
                Elem{
                    txtGroup: tmodel.dataColumn(styleData.index,0)
                    txtContent: tmodel.dataColumn(styleData.index,1)
                    txtHTML: tmodel.dataColumn(styleData.index,2)
                    txtUpdated: tmodel.dataColumn(styleData.index,3)
                    hasChaild: tmodel.dataColumn(styleData.index,4)
                    topLevel: tmodel.dataColumn(styleData.index,5)

                    onExpand: {
                        treeView.isExpanded(index)?treeView.collapse(index):treeView.expand(index)
                    }
                }
            }
        }

        }

//    onClosing: {
//            close.accepted = false
            //mainWin.visible = false
//        dialogDebug.mess="back"
//        dialogDebug.open()

//    }

    Control2_11.Dialog{
        id: dialogDebug
        modal: true

        property string mess:""

        title: mess
        standardButtons: Control2_11.Dialog.OK

//        onAccepted: {}//del(styleData.index)
//        onRejected:
    }

}

