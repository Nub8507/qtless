#include "widget.h"
#include <QtWidgets>
#include "namevalidator.h"
#include "webview.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
{
    pDateTimeEdit=new QDateTimeEdit(QDateTime::currentDateTime());
    pDateTimeEdit->setDisplayFormat("dd.MM.yyyy hh:mm:ss");

    QPushButton* pbtnDate=new QPushButton("Обновить");

    connect(pbtnDate,SIGNAL(clicked()),SLOT(btnClick()));

    QLabel* plblName=new QLabel("&Name");
    QLineEdit* ptxtName=new QLineEdit;
    NameValidator* pnVal=new NameValidator(ptxtName);
    ptxtName->setValidator(pnVal);
    plblName->setBuddy(ptxtName);

    pWebV=new WebView();

    QHBoxLayout* phbLayoutDate=new QHBoxLayout;
    phbLayoutDate->addWidget(pDateTimeEdit);
    phbLayoutDate->addWidget(pbtnDate);

    QVBoxLayout* pvbLayout=new QVBoxLayout;
    pvbLayout->addLayout(phbLayoutDate);
    pvbLayout->addWidget(plblName);
    pvbLayout->addWidget(ptxtName);
    pvbLayout->addWidget(pWebV);
    this->setLayout(pvbLayout);

}

Widget::~Widget()
{

}

void Widget::showHTML(QString url)
{
    if(pWebV!=nullptr)
        pWebV->showHTML(url);
}

void Widget::btnClick()
{
    //
    if(pDateTimeEdit==nullptr) return;
    //
    pDateTimeEdit->setDateTime(QDateTime::currentDateTime());
    //
}
