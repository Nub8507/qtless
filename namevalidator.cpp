#include "namevalidator.h"
#include <QtWidgets>

QValidator::State NameValidator::validate(QString& str,int& pos) const
{
    QRegExp rxp=QRegExp("[0-9 ]");
    //
    if(str.contains(rxp)){
        return QValidator::Invalid;
    }
    return QValidator::Acceptable;
};
