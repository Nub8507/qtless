#ifndef ROREPLICA_H
#define ROREPLICA_H

#include "authortreelist.h"
#include "contentelement.h"

#include <QObject>

#include <QAbstractItemModel>
#include <qremoteobjectdynamicreplica.h>

class RoReplica:public QObject
{
	Q_OBJECT
public:
	RoReplica(QSharedPointer<QRemoteObjectDynamicReplica> ptr,QObject* parent=nullptr);
	~RoReplica();

	bool isReplicaValid();

signals:
	void connectionLost();
	void r_startParce(QString txtHTML);
	void r_AddAuthor(QString Author);
	void r_UpdateAll();
	void r_SaveAll();
	void r_userCheckedUpdate(QString index);
	void r_del(QString index);
	void sendTrayMessage(const QString title,const QList<QString> txtList);
	void initialized();
        void initUserData(QList<QString> Authors,QVector<ContentElement> dataList);
        void ElementChanged(ContentElement elem);
        void ElementDeleted(ContentElement elem);

private slots:
	void initConnection_slot();
	void ConnectionStatus_slot(QRemoteObjectReplica::State state, QRemoteObjectReplica::State oldState);
	void startParce(QString txtHTML);
	void AddAuthor(QString Author);
	void UpdateAll();
	void SaveAll();
	void userCheckedUpdate(QModelIndex index);
	void del(QModelIndex index);
	void r_sendTrayMessage(const QString title,const QList<QString> txtList);
	void r_initUserData(QList<QString> Authors,QList<QString> dataList);
	void r_ElementChanged(QString strElem);
	void r_ElementDeleted(QString strElem);


private:
	QSharedPointer<QRemoteObjectDynamicReplica> reptr;

};

#endif // ROREPLICA_H
