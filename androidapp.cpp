#include "androidapp.h"
#include "interface.h"
#include "resource.h"
#include "roreplica.h"
#include "saveload.h"
#include "timecontrol.h"
#include "settings.h"

#include <QGuiApplication>
#include <QAndroidService>
#include <QAndroidJniObject>
#include <QtAndroid>
#include <QQmlApplicationEngine>
#include <QCoreApplication>
#include <QQuickWindow>
#include <QRemoteObjectRegistryHost>
#include <QThread>


AndroidApp::AndroidApp(int argc, char *argv[], QObject *parent) : QObject(parent)
{
	//
	QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
	ErrorExit=0;
	//
	if(argc>1){
		ThisIsService=true;
		app=new QAndroidService(argc,argv);
		//
		initServer();
		//
	}else {
		ThisIsService=false;
		app=new QGuiApplication(argc,argv);
		//
		initReplica();
		//
#ifdef QT_DEBUG
		initServer();
#endif
		//
	}
	//
}

AndroidApp::~AndroidApp()
{
	//
	delete app;
	//
}

int AndroidApp::exec()
{
	//
	if(ThisIsService){
		auto appl=static_cast<QAndroidService *>(app);
		if(ErrorExit!=0){
			return ErrorExit;
		}
		//
		return appl->exec();
		//
	}else {
		auto appl=qobject_cast<QGuiApplication *>(app);
		if(ErrorExit!=0){
			return ErrorExit;
		}
		//
		return appl->exec();
		//
	}
	//
}

void AndroidApp::connectionLost()
{
	//
	ErrorExit=1;
	qobject_cast<QCoreApplication*>(app)->quit();
	//
}

void AndroidApp::sendDebugMessage()
{
	//
	emit sendDebugMessage("сообщ об ошибке");
	//
}

void AndroidApp::sendDebugMessage(QString mess)
{
	//
	emit debugDialog(QVariant::fromValue(mess));
	//
}

void AndroidApp::initReplicaConnection()
{
	//
	if (Data!=nullptr){
		window->setProperty("tmodel",QVariant::fromValue(Data->getTLModel()));
	}
	//
}

void AndroidApp::sendTrayMessage(const QString title, const QList<QString> txtList)
{
	//
	QString text;
	//
	int min=std::min(4,txtList.count());
	for(int i=0;i<min;++i)
		text=text+txtList.at(i)+"\n";
	//
	if(min<txtList.count())
		text=text+"...";
	else
		text=text.trimmed();
	//
	QAndroidJniObject javaTitle = QAndroidJniObject::fromString(title);
	QAndroidJniObject javaMessage = QAndroidJniObject::fromString(text);//"(Ljava/lang/String;Ljava/lang/string;)V",javaTitle.object<jstring>(),
	QAndroidJniObject::callStaticMethod<void>("ru/net/artpeople/slview/QtSLViewService",
										"notify",
										"(Ljava/lang/String;)V",
										javaMessage.object<jstring>());
	//
}

void AndroidApp::initServer()
{
	//
#ifdef QT_DEBUG
	Resource* Data;
#endif
	//
	Data= new Resource(app);
	Timer= new timeControl(app);
	File=new SaveLoad(app);
	//
    Data->setAuthors(File->LoadAuthors());
    {
        auto bookList=File->LoadSavedList();
        Data->initList(bookList);
    }
    //
    Timer->setResource(Data);
	//
#ifdef QT_DEBUG
	Timer->startWork(3*60*1000);//3 минуты
#else
	Timer->startWork(120*60*1000);//120 минут
#endif
	//
	regNode=new QRemoteObjectRegistryHost(QUrl(QStringLiteral("local:slview_registry")),app);
#ifdef QT_DEBUG
	Node2= new QRemoteObjectHost(QUrl(QStringLiteral("local:slview")), QUrl(QStringLiteral("local:slview_registry"))); // create node that will host source and connect to registry
	//
	if(!Node2->enableRemoting(&ROServerClass)){
		ErrorExit=1;
		return;
	}
#else
	Node= new QRemoteObjectHost(QUrl(QStringLiteral("local:slview")), QUrl(QStringLiteral("local:slview_registry"))); // create node that will host source and connect to registry
	//
	if(!Node->enableRemoting(&ROServerClass)){
		ErrorExit=1;
		return;
	}
#endif
	//
	QObject::connect(&ROServerClass,SIGNAL(startParce(QString)),Data,SLOT(startParce(QString)));
	QObject::connect(&ROServerClass,SIGNAL(AddAuthor(QString)),Data,SLOT(AddAuthor(QString)));
	QObject::connect(&ROServerClass,SIGNAL(updateAll()),Timer,SLOT(UpdateAll()));
	QObject::connect(&ROServerClass,SIGNAL(SaveAll()),Data,SLOT(SaveAll()));
	QObject::connect(&ROServerClass,SIGNAL(userCheckedUpdate(ContentElement)),Data,SLOT(userCheckedUpdate(ContentElement)));
	QObject::connect(&ROServerClass,SIGNAL(del(ContentElement)),Data,SLOT(del(ContentElement)));
	QObject::connect(&ROServerClass,SIGNAL(AllContentData()),Data,SLOT(getAllContentData()));
	QObject::connect(Data,SIGNAL(ElementChanged(AuthorTreeList::NodeItem)),&ROServerClass,SLOT(ElementChanged(AuthorTreeList::NodeItem)));
	QObject::connect(Data,SIGNAL(ElementDeleted(AuthorTreeList::NodeItem)),&ROServerClass,SLOT(ElementDeleted(AuthorTreeList::NodeItem)));
	//
	QObject::connect(Data,SIGNAL(initSaveList(QList<AuthorTreeList::NodeItem>,SaveLoad::Direction)),File,SLOT(SaveTextsList(QList<AuthorTreeList::NodeItem>,SaveLoad::Direction)));
	QObject::connect(Data,SIGNAL(initSaveAuthors(QList<QString>,SaveLoad::Direction)),File,SLOT(SaveAuthor(QList<QString>,SaveLoad::Direction)));
	QObject::connect(Data,SIGNAL(ModelUpdated()),Data,SLOT(SaveList()));
	QObject::connect(Data,SIGNAL(finishAuthorChange()),Data,SLOT(SaveAuthors()));
	QObject::connect(Data,SIGNAL(parceFinish(QList<ContentElement>&)),Timer,SLOT(parceFinish()));
//	QObject::connect(Data,SIGNAL(sendTrayMessage(const QString,const QList<QString>)),&ROServerClass,SIGNAL(r_sendTrayMessage(const QString,const QList<QString>)));
	QObject::connect(Data,SIGNAL(sendTrayMessage(const QString,const QList<QString>)),this,SLOT(sendTrayMessage(const QString,const QList<QString>)));
	QObject::connect(Data,SIGNAL(AllContentData(QList<QString>,QList<AuthorTreeList::NodeItem>)),&ROServerClass,SLOT(initUserData(QList<QString>,QList<AuthorTreeList::NodeItem>)));
	//
	QObject::connect(Timer,SIGNAL(startParce(QString)),Data,SLOT(startParce(QString)));
	QObject::connect(Timer,SIGNAL(saveList()),Data,SLOT(SaveList()));
	QObject::connect(Timer,SIGNAL(debugMessage(const QString,const QList<QString>)),this,SLOT(sendTrayMessage(const QString,const QList<QString>)));
	//
#ifdef QT_DEBUG
	this->Data2=Data;
#endif
	//
}

void AndroidApp::initReplica()
{
	//
	Gui=new Interface(app);
	Data=new Resource(app);
	//
	engine=new QQmlApplicationEngine(app);
	const QUrl url(QStringLiteral("qrc:/main_a.qml"));
	engine->load(url);
	//
	window=dynamic_cast<QQuickWindow*>(engine->rootObjects()[0]);
//		initReplicaConnection();
	//
	Node= new QRemoteObjectHost(QUrl(QStringLiteral("local:slview_registry")));
	Node->connectToNode(QUrl(QStringLiteral("local:slview")));
	dynRepPtr.reset(Node->acquireDynamic("SLViewQtRemoute"));
	ROReplicaClass=new RoReplica(dynRepPtr,app);
	//
	QObject::connect(window,SIGNAL(startParce(QString)),ROReplicaClass,SLOT(startParce(QString)));
	QObject::connect(window,SIGNAL(updateChecked(QVariant)),Gui,SLOT(updateChecked(QVariant)));
	QObject::connect(window,SIGNAL(updateAll()),Gui,SLOT(UpdateAll()));
	QObject::connect(window,SIGNAL(del(QVariant)),Gui,SLOT(del(QVariant)));
//		t = QObject::connect(window,SIGNAL(quit()),app,SLOT(quit()));
	//
	QObject::connect(ROReplicaClass,SIGNAL(sendTrayMessage(const QString,const QList<QString>)),Gui,SLOT(sendTrayMessage(const QString,const QList<QString>)));
	//
	QObject::connect(Gui,SIGNAL(AddAuthor(QString)),ROReplicaClass,SLOT(AddAuthor(QString)));
	QObject::connect(Gui,SIGNAL(sendTrayMessage(QVariant,QVariant)),window,SLOT(setSysTrayMessge(QVariant,QVariant)));
	QObject::connect(Gui,SIGNAL(userCheckedUpdate(QModelIndex)),ROReplicaClass,SLOT(userCheckedUpdate(QModelIndex)));
	QObject::connect(Gui,SIGNAL(del(QModelIndex)),ROReplicaClass,SLOT(del(QModelIndex)));
	QObject::connect(Gui,SIGNAL(del(QModelIndex)),Data,SLOT(del(QModelIndex)));
	QObject::connect(Gui,SIGNAL(updateAll()),ROReplicaClass,SLOT(UpdateAll()));
	//
	QObject::connect(ROReplicaClass,&RoReplica::initUserData,Data,&Resource::initAllData);
    QObject::connect(ROReplicaClass,&RoReplica::ElementChanged,Data,&Resource::elementChange);
    QObject::connect(ROReplicaClass,&RoReplica::ElementDeleted,Data,&Resource::elementDelete);
	QObject::connect(ROReplicaClass,&RoReplica::connectionLost,this,&AndroidApp::connectionLost);
	//
    QObject::connect(Data,&Resource::FinishDataLoading,this,&AndroidApp::initReplicaConnection);
//		QObject::connect(ROReplicaClass,&RoReplica::initialized,this,&AndroidApp::initReplicaConnection);
	//
	QObject::connect(app,SIGNAL(aboutToQuit()),ROReplicaClass,SLOT(SaveAll()));
	//
#ifndef QT_DEBUG
	QAndroidJniObject::callStaticMethod<void>("ru/net/artpeople/slview/QtSLViewService",
												  "startMyService",
												  "(Landroid/content/Context;)V",
												  QtAndroid::androidActivity().object());
#endif
	//
}
