#ifndef INTERFACE_H
#define INTERFACE_H

#include "contentelement.h"

#include <QAbstractItemModel>
#include <QObject>
#include <QVariant>

class Interface : public QObject
{
    Q_OBJECT
public:
	explicit Interface(QObject *parent = nullptr);

signals:
	void UserCheckedUpdate(ContentElement index);							//вызывается для передачи
																			//найденой ссылки на которую щелкнул пользователь
	void SendTrayMessage(QVariant title,QVariant text);						//вызывается для отправлки сообщения о новых произведениях
																			//title- заголовок, text - сообщение
																			//тип параметров QString
//	void AddAuthor(QString author);
	void Del(ContentElement index);											//вызывается для передачи
																			//найденой ссылки на удаленного автора
	void UpdateAll();														//вызывается при необходимости обновить всех авторов
	void LoadSettings(QVariant toF,QVariant txtLogin,QVariant txtPassword);	//отправляет информацию о способе хранения данных авторов
	void StartParce(QString txtHTML);

public slots:
	void updateChecked(const QVariant index);								//вызывается при щелчке пользователя на значке обновления элемента
																			//ожидает индекс модели от QML
    void sendTrayMessage(const QString title,const QVector<QString> txtList);	//вызывается при необходимости отправить всплывающее сообщение
	void del(const QVariant index);											//вызывается при щелчке пользователя на значке удаления элемента
																			//ожидает индекс модели от QML
	void updateAll();														//вызывается при щелчке пользователя на значке обновить все
	void needLoadSettings();												//вызывается при открытии настроек пользователя
	void saveSettings(QVariant toF,QVariant txtLogin,QVariant txtPassword);	//вызывается при сохранении информации о хренении данных авторов
	void startParce(QString txtHTML);										//вызывается при щелчке пользователя на значке добавления автора

private:


};

#endif // INTERFACE_H
