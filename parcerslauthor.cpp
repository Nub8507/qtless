#include "parcerslauthor.h"

#include <contentelement.h>
#include <QString>
#include <QStringList>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QTextCodec>

using namespace ParcerSL;

ParcerSLAuthor::ParcerSLAuthor(QObject *parent)
    : QObject{parent},pHtml{new  QNetworkAccessManager(this)}
{
}

ParcerSLAuthor::ParcerSLAuthor(QString HTML, QObject *parent)
    : QObject{parent},HTMLSource{HTML},pHtml{new  QNetworkAccessManager(this)}
{
}

ParcerSLAuthor::ParcerSLAuthor(ContentElement& p, QString HTML, QObject *parent)
    : QObject{parent},HTMLSource{HTML},Parent{p},pHtml{new  QNetworkAccessManager(this)}
{
}

void ParcerSLAuthor::slotHTMLFinish()
{
    //получить результат запроса html
    QNetworkReply* reply=qobject_cast<QNetworkReply *>(sender());
    //при ошибке устанавливаем флаги ошибки
    if(reply->error()!=QNetworkReply::NoError){
        reply->deleteLater();
        downloading=false;
        status=false;
        PageType=ErrorHTML;
        emit finished(false);
        return;
    }
    //прочитаем результат запроса
    QByteArray content= reply->readAll();
    //найдем кодировку
    QByteArray Cont=FindCharset(content);
    //переведем результат из потока данных в строку
    QTextCodec *codec = QTextCodec::codecForName(Cont);
    QString st=codec->toUnicode(content.data());
    //теперь результат запроса можно удалять
    reply->deleteLater();
    //начинаем расшифровку
    downloading=false;
    status=ParceContent(st);
    --NumChildren;
    //если нет дочерних процессов сообщить о завершении расшифровки
    if (NumChildren==0){
        emit finished(status);
    }
    //
}

void ParcerSLAuthor::Start()
{
    //если адрес страницы не заполнен, ничего не делаем
    if(HTMLSource.isEmpty()){status=false;return;}
    //если идет загрузка страницы, ничего не делаем
    if(downloading)return;
    //создаем html запрос
    QUrl url(HTMLSource);
    QNetworkRequest request(url);
    //пробуем получить результат запроса
    downloading=true;
    QNetworkReply* reply=pHtml->get(request);
    connect(reply,SIGNAL(finished()),this,SLOT(slotHTMLFinish()));
    //
}

void ParcerSLAuthor::RemoveTags(QString &source)
{
    //
    Range pos;
    //
    while (pos.StartPos!=-1||pos.EndPos!=-1){//производим удаление пока есть открывающие и закрывающие кавычки
        pos.StartPos=source.indexOf("<",0,Qt::CaseInsensitive);//находим положение следующей кавычки
        pos.EndPos=source.indexOf(">",0,Qt::CaseInsensitive);
		if(pos.EndPos<pos.StartPos){
			if(pos.EndPos==-1)
				source.remove(pos.StartPos,source.length()-pos.StartPos);//если закрывающей кавычки нет удаляем все до конца строки
			else
				source.remove(0,pos.EndPos+1);//если сначала идет закрывающая кавычка удаляем все с начала строки
		}
		if(pos.EndPos>pos.StartPos)source.remove(pos.StartPos,pos.lenght()+1);//иначе удаляем область между кавычками
    }
    //
}

QByteArray ParcerSLAuthor::FindCharset(QByteArray &source)
{
    //
    Range pos;
    //пытаемся найти определение кодировки
    pos.StartPos=source.indexOf("charset=");
    if(pos.StartPos==-1)return QByteArray("Windows-1251");//если не найдена возвращаем Windows-1251
    //
    pos.StartPos+=8;//если найдена смещаем указатель на начало данных
    //
    QByteArray rez;
    auto start=source.cbegin()+pos.StartPos;
    while(start!=source.cend()&&(*start==' '||*start=='\"'))++start;//удаляем начальные пробелы и кавычки
    //копируем данные до символа-разделителя
    for (auto p=start;p!=source.cend()&&(*p!=' '&&*p!='\"'&&*p!='>');++p){rez.push_back(*p);}
    return rez;
}

bool ParcerSLAuthor::ParceContent(QString& source)
{
    //
    QStringList List=StrToList(source);                                     //разбиваем исходный текст страницы на строки
    //
    PageType=FindPageType(List);                                            //определяем вид страницы
    //
    ContentElement elem;
    //
    if(PageType==Main){                                                     //если это начальная страница автора
		QString Author=FindAuthor(List);
		RemoveTags(Author);
		elem.Author=Author.trimmed();					                    //находим имя автора
		elem.HTMLAuthorAdress=HTMLSource.trimmed();							//сохраняем адрес страницы автора
        if(elem.Author.isEmpty()){                                          //если не удалось получить имя автора
            PageType=ErrorParce;                                            //тогда считаем что неправильно расшифровали страницу
			return false;													//и прерываем расшифровку
        }
//		elem.level=1;														//Это первый уровень иерархии
		ParceResult.push_back(elem);										//сохраняем заполненный элемент
	}else if(PageType==Group||PageType==Text)								//если это группа произведений или произведение
		elem=Parent;														//заполняем родителя и продолжаем расшифровку
	else return false;														//если не смогли определить тип страницы прерываем расшифровку
    //
	if(PageType==Main||PageType==Group){									//для главной страници и группы
		Range ContentsList=FindContentList(List);							//находим список произведений
        //
		return ParceMainGroupPage(List,ContentsList,elem);					//начинаем расшифровку списка
        //
	}else {																	//для произведения
		return ParceTextPage(List,Parent);									//сразу начинаем расшифровку
    }
    //
}

HTMLPageType ParcerSLAuthor::FindPageType(const QStringList &List)
{
    //
    Range main=FindContentList(List);                   //пробуем найти список произведений
    if(main.StartPos!=-1){                              //если список найден
        if(Parent.isEmpty())return HTMLPageType::Main;  //и родитель не заполнен - это главная страница
        else return HTMLPageType::Group;                //иначе это группа
    }else {                                             //если список произведений не найден
        Range text=FindTextList(List);                  //пробуем найти информацию о произведении
        if(text.StartPos!=-1)return HTMLPageType::Text;}//если она найдена то это произведение
    //
    return HTMLPageType::Unknown;                       //иначе неизвестная страница
    //
}

QString ParcerSLAuthor::FindAuthor(const QStringList &List) //возможно переделать поиск!!!!!
{
    //
	const int NumOfAuthorString=8;							//в 8 строке должно быть имя автора
	if(List.size()<NumOfAuthorString+1)return "";			//если строк меньше - автор не найден
    //
    QString t=List[NumOfAuthorString];
    //
	int StartPos=t.indexOf("<h3>");							//пытаемся найти начало имени
    if(StartPos==-1)return "";
    //
	int EndPos=t.indexOf(":<",StartPos+4);					//и окончание
    if (EndPos==-1)return "";
    //
	return t.mid(StartPos+4,EndPos-(StartPos+4)).trimmed();
    //
}

ParcerSLAuthor::Range ParcerSLAuthor::FindContentList(const QStringList &List)
{
    //
    Range t;
    //
    t.StartPos=List.indexOf(QRegExp{"<!--------- Блок ссылок на произведения ------------>\r"});
    t.EndPos=List.indexOf(QRegExp{"<!--------- Подножие ------------------------------->\r"});
    //
    return t;
    //
}

ParcerSLAuthor::Range ParcerSLAuthor::FindTextList(const QStringList &List)
{
    Range t;
    //
    QRegExp q{"<!---- Блок описания произведения \\(слева вверху\\) ----------------------->\r"};
    t.StartPos=List.indexOf(q);
    t.EndPos=List.indexOf(QRegExp{"<!---------- Кнопка вызова Лингвоанализатора -------->\r"});
    //
    return t;
}

bool ParcerSLAuthor::ParceMainGroupPage(const QStringList &List, ParcerSLAuthor::Range Pos,const ContentElement& pelem)
{
    //
	ContentElement elem{pelem};																	//заполняем родительские данные элемента
    //
	auto StartPos{List.begin()+Pos.StartPos+1};
	auto EndPos{List.begin()+Pos.EndPos};
	#ifdef QT_DEBUG
		int i=Pos.StartPos+1;
	#endif
	for(auto pos=StartPos;pos!=EndPos;++pos){													//читаем последовательно строки в списке
        QString t=*pos;
		#ifdef QT_DEBUG
			qDebug()<<"строка "<<i;
			++i;
		#endif
		if(t.left(8)=="</small>"){ QString t2=*(pos+=2); elem=ParceGroupString(t,t2,elem);}		//если строка это группа, тогда читаем еще 2 строки и обрабатываем прочитанное
		if(t.left(8)=="<DL><DT>") ParceBookString(t,elem);										//если произведение начинаем расшифровку
    }
    //
    return true;
    //
}

bool ParcerSLAuthor::ParceTextPage(const QStringList &List, ContentElement& elem)
{
    //
	Range linepos=FindTextList(List);																	//пытаемся найти описание произведения
    //
	if (linepos.StartPos==-1)return false;																//еcли не найдено, то ошибка расшифровки
    //
    Range pos;
    QString source;
    pos.StartPos=-1;
    for(auto p=List.begin()+linepos.StartPos;(p!=List.begin()+linepos.EndPos)&&pos.StartPos==-1;++p){   //просматриваем поледовательно строки описания произведения
        pos.StartPos=(*p).indexOf("изменен:",0,Qt::CaseInsensitive);                                    //пока не найдем строку с датой
        source=*p;                                                                                      //запоминаем ее
    }
    if(pos.StartPos==-1)return false;                                                                   //если строку с датой не нашли - ошибка расшифровки
    pos.EndPos=source.indexOf(" ",pos.StartPos,Qt::CaseInsensitive);
    pos.swap(1);
    pos.EndPos=source.indexOf(".",pos.StartPos,Qt::CaseInsensitive);
    QString t=source.mid(pos.StartPos,pos.lenght());                                                    //выделяем дату из строки
    //
    QDate d=QDate::fromString(t,"dd/MM/yyyy");
	elem.LastUpdate=d;
//    elem.level=3;                                                                                       //уровень иерархии 3
    ParceResult.push_back(elem);                                                                        //сохраняем результат расшифровки
    //
    return true;
    //
}

void ParcerSLAuthor::ParceBookString(QString &source, const ContentElement &pelem)
{
    //
    Range pos;
	pos.StartPos=source.indexOf("HREF=",0,Qt::CaseInsensitive);											//пытаемя найти начало блока ссылки на страницу книги
    if (pos.StartPos==-1)return;
	pos.EndPos=source.indexOf(">",pos.StartPos,Qt::CaseInsensitive);									//пытаемя найти конец блока ссылки на страницу книги
    if (pos.EndPos==-1)return;
    pos.StartPos+=5;
	QString HTMLContentAdress=source.mid(pos.StartPos,pos.lenght());									//получаем HTML адрес книги
    //
    pos.swap(1);
    //
	pos.EndPos=source.indexOf("</A>",pos.StartPos,Qt::CaseInsensitive);									//пытаемся найти конец названия книги
    if (pos.EndPos==-1)return;
	QString Content=source.mid(pos.StartPos,pos.lenght());												//получаем название книги
    RemoveTags(Content);
    pos.swap();
    //
	pos.EndPos=source.indexOf("<DD>",pos.StartPos,Qt::CaseInsensitive);									//пытаемся найти начало описания книги
    QString Anotation="";
    if (pos.EndPos!=-1){
        pos.swap(4);
		pos.EndPos=source.indexOf("</DL>",pos.StartPos,Qt::CaseInsensitive);							//пытаемся найти конец описания книги
		if(pos.EndPos!=-1) Anotation=ParceAnotation(source.mid(pos.StartPos,pos.lenght()));				//если описание найдено начинаем его расшифровку
    }
    RemoveTags(Anotation);
    //
	ContentElement elem{pelem};
	elem.Content=Content.trimmed();
	elem.Anotation=Anotation.trimmed();
	elem.HTMLContentAdress=HTMLContentAdress.trimmed();
//    ParceResult.push_back(elem);
    //
	if(!elem.HTMLContentAdress.isEmpty()){																//если html адрес текста найден
		ParcerSLAuthor* gParcer=new ParcerSLAuthor(elem,elem.HTMLAuthorAdress+elem.HTMLContentAdress,this);	//создаем обработчик расшифровки
        connect(gParcer,SIGNAL(finished(bool)),this,SLOT(ChildFinish(bool)));
		++NumChildren;																						//добавляется новый процесс расшифровки
		gParcer->Start();																					//запускаем расшифровку такста книги
    }
    //
}

QString ParcerSLAuthor::ParceAnotation(QString source)
{
    //
    Range pos;
    pos.StartPos=0;
	pos.EndPos=source.indexOf("<DD>",0,Qt::CaseInsensitive);												//получаем первый блок описания произведения
    QString t="";
    while (pos.EndPos!=-1){
		t+=ParceAnotation(source.mid(pos.StartPos,pos.lenght()))+"\n";										//расшифровываем найденный блок описания
        pos.swap(4);
		pos.EndPos=source.indexOf("<DD>",pos.StartPos,Qt::CaseInsensitive);									//получаем следующий блок описания
    }
	pos.EndPos=source.indexOf("/img/",pos.StartPos,Qt::CaseInsensitive);									//не добавляем в описание количество рисунков
    if (pos.EndPos==-1)return t==""?source:t;
    return t;
    //
}

ContentElement ParcerSLAuthor::ParceGroupString(QString s1, QString s2, const ContentElement &pelem)
{
    //
    ContentElement elem=pelem;
    Range pos;
	pos.StartPos=s1.indexOf("<a name=gr",0,Qt::CaseInsensitive);												//пробуем найти начало блока <a name=gr???>
	if (pos.StartPos==-1)return elem;
    //
	pos.EndPos=s1.indexOf(">",pos.StartPos,Qt::CaseInsensitive);												//находим конец группы
    pos.swap(1);
    //
	pos.EndPos=s1.indexOf("<a href=",pos.StartPos,Qt::CaseInsensitive);											//находим начало HTML адреса группы
    elem.HTMLGroupAdress="";
    if(pos.EndPos!=-1){
        pos.swap(8);
        pos.EndPos=s1.indexOf(">",pos.StartPos,Qt::CaseInsensitive);
		if(pos.EndPos!=-1)elem.HTMLGroupAdress=s1.mid(pos.StartPos,pos.lenght()).trimmed();								//получаем HTML адрес
        pos.swap(1);
    }
    //
	pos.EndPos=s1.indexOf("</a>",pos.StartPos,Qt::CaseInsensitive);												//находим конец названия группы
    if (pos.EndPos==-1)pos.EndPos=s1.indexOf("<gr",pos.StartPos,Qt::CaseInsensitive);
    if(pos.EndPos==-1)pos.EndPos=s1.length();
    //
	QString Group=s1.mid(pos.StartPos,pos.lenght());
	RemoveTags(Group);
	elem.Group=Group.trimmed();																					//получаем название группы
    //
    bool GroupIsNull=false;
	if(elem.HTMLGroupAdress.left(6)=="/type/"||elem.HTMLGroupAdress=="")GroupIsNull=true;						//проверяем есть ли произведения на отдельной странице группы
    //
    RemoveTags(s2);
	elem.Anotation=s2.trimmed();																				//вторая строка - описание группы
//    elem.level=2;                                                                                               //уровень иерархии 2
    ParceResult.push_back(elem);
    //
	if(!GroupIsNull){																							//если тексты расположены на странице группы
		ParcerSLAuthor* gParcer=new ParcerSLAuthor(elem,elem.HTMLAuthorAdress+elem.HTMLGroupAdress,this);		//создаем обработчик страницы группы
        connect(gParcer,SIGNAL(finished(bool)),this,SLOT(ChildFinish(bool)));
		++NumChildren;																							//увеличиваем количество дочерних расшифровок
		gParcer->Start();																						//запускаем расшифровку страницы группы
    }
    //
    return elem;
    //
}

void ParcerSLAuthor::ChildFinish(bool Status)
{
    //
	ParcerSLAuthor* reply=qobject_cast<ParcerSLAuthor *>(sender());
    //
	if(reply->parceStatus()){											//если расшифровка удалась
        for(auto p:reply->getResult()){
			ParceResult.push_back(p);									//добавляем в общий список результатов расшифрованные элементы
        }
    }
    //
	reply->deleteLater();												//класс расшифровки больше не нужен
    //
	--NumChildren;														//дочерний процесс завершен
    //
	if (NumChildren==0){												//если све дочерние процессы завершены
		status=status&true;												//статус расшифровки - успешно
		emit finished(Status);											//расшифровка блока завершена
    }
    //
}

