package ru.net.artpeople.slview;

import android.os.Bundle;
import android.content.Context;
import android.content.Intent;
import android.content.BroadcastReceiver;

import ru.net.artpeople.slview.QtSLViewService;

public class MyBroadcastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Intent startServiceIntent = new Intent(context, QtSLViewService.class);
        context.startService(startServiceIntent);
    }
}
