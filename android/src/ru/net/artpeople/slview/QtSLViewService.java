
package ru.net.artpeople.slview;

import android.content.Context;
import android.content.Intent;
import android.app.PendingIntent;
import android.app.Notification;
import android.app.NotificationManager;

import ru.net.artpeople.slview.MainApplicationClass;
import org.qtproject.qt5.android.bindings.QtService;

public class QtSLViewService extends QtService
{
    private static NotificationManager m_notificationManager;
    private static Notification.Builder m_builder;
    private static QtSLViewService m_instance;

    public QtSLViewService()
    {
        m_instance = this;
    }

    public static void startMyService(Context ctx) {

        ctx.startService(new Intent(ctx, QtSLViewService.class));

    }

//    public static void notify(String title,String message)
    public static void notify(String message)
    {

        Intent intent = new Intent(m_instance, MainApplicationClass.class);
        PendingIntent contentIntent = PendingIntent.getActivity(m_instance,
                0, intent,0);

        Notification.Builder builder = new Notification.Builder(m_instance)
                        .setTicker("Обновились произведения")
                        .setContentTitle("Обновились произведения")
                        .setContentText("")
                        .setSmallIcon(R.drawable.icon)
                        .setContentIntent(contentIntent)
                        .setDefaults(Notification.DEFAULT_SOUND)
                        .setWhen(System.currentTimeMillis())
                        .setAutoCancel(true);
//                        .addAction(R.drawable.icon, "Запустить активность",
//                                contentIntent).setAutoCancel(true)


        Notification notification = new Notification.BigTextStyle(builder)
                        .bigText(message).build();

        NotificationManager notificationManager = (NotificationManager)m_instance.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(1, notification);
    }

}
