#pragma once
#ifndef REGEXTTEST_H
#define REGEXTTEST_H

#include <QWidget>

class QLineEdit;
class QLabel;
class QPushButton;

class RegExtTest : public QWidget
{
    Q_OBJECT
public:
    explicit RegExtTest(QWidget *parent = nullptr);

private:
    QLineEdit* ptxtRegExp;
    QLineEdit* ptxtExpr;
    QLabel*    plblResult;
    QPushButton* pbtnExec;

public slots:
    void btnClick();
};

#endif // REGEXTTEST_H
