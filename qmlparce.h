#ifndef QMLPARCE_H
#define QMLPARCE_H

#include <QObject>
#include <QQmlApplicationEngine>
#include <QVariant>

class AuthorTreeList;
class AuthorTreeModel;
struct ContentElement;


class QMLParce : public QObject
{
    Q_OBJECT
public:
    explicit QMLParce(QObject *parent = nullptr);
    ~QMLParce();

    AuthorTreeList* T;
    AuthorTreeModel* TVModel;
    QList<ContentElement>t{};

signals:
    void TTSetVisible(QVariant vis);

public slots:
    void startParce(QString txtHTML);
    void slotFinished();
};

#endif // QMLPARCE_H

