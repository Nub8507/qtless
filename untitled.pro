#-------------------------------------------------
#
# Project created by QtCreator 2018-10-14T22:22:20
#
#-------------------------------------------------

QT       += core gui quick

greaterThan(QT_MAJOR_VERSION, 4): QT += network quickwidgets widgets

android {

SOURCES += \
    androidapp.cpp \
    roreplica.cpp \
    roserver.cpp

HEADERS += \
    androidapp.h \
    roreplica.h \
    roserver.h

QT += androidextras remoteobjects

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android

REPC_SOURCE = SLViewQtRemoute.rep
#REPC_REPLICA = SLViewQtRemoute.rep

DISTFILES += \
    android/src/ru/net/artpeople/slview/QtSLViewService.java \
    android/src/ru/net/artpeople/slview/MainApplicationClass.java \
    android/src/ru/net/artpeople/slview/MyBroadcastReceiver.java \
    android/res/drawable/icon.png \
    android/AndroidManifest.xml \
    android/gradle.properties \
    android/build.gradle

#    android/gradle/wrapper/gradle-wrapper.jar \
#    android/gradlew \
#    android/res/values/libs.xml \
#    android/build.gradle \
#    android/gradle/wrapper/gradle-wrapper.properties \
#    android/.gradle/4.6/fileChanges/last-build.bin \
#    android/.gradle/4.6/fileHashes/fileHashes.bin \
#    android/.gradle/4.6/fileHashes/fileHashes.lock \
#    android/.gradle/vcsWorkingDirs/gc.properties
#    android/gradlew.bat
}

win*{

#CONFIG(debug, debug|release) {
#    DESTDIR = $$OUT_PWD/../../SLViewDebug
#} else {
#    DESTDIR = $$OUT_PWD/../../SLViewRelease
#}
#
#CONFIG(debug, debug|release) {
#    QMAKE_POST_LINK = $$(QTDIR)/bin/windeployqt --qmldir $$PWD $$OUT_PWD/../../SLViewDebug
#} else {
#    QMAKE_POST_LINK = $$(QTDIR)/bin/windeployqt --qmldir $$PWD $$OUT_PWD/../../SLViewRelease
#}

SOURCES += \
    winapp.cpp

HEADERS += \
    winapp.h

}

TARGET = SLView
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
    htmlsender.cpp \
    main.cpp \
    parcerslauthor.cpp \
    authortreelist.cpp \
    authortreemodel.cpp \
    interface.cpp \
    saveload.cpp \
    resource.cpp \
    timecontrol.cpp


HEADERS += \
    contentelement.h \
    htmlsender.h \
    parcerslauthor.h \
    authortreelist.h \
    authortreemodel.h \
    interface.h \
    saveload.h \
    resource.h \
    settings.h \
    timecontrol.h

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    qml.qrc



