
#ifdef ANDROID

#include <androidapp.h>

#endif

#ifndef ANDROID
#include <winapp.h>

#endif

 int main(int argc, char *argv[])
{

#ifdef ANDROID

	 AndroidApp app{argc,argv};

#endif

#ifndef ANDROID

	WinApp app{argc,argv};

#endif
	return app.exec();
}

