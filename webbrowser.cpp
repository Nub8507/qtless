#include "webbrowser.h"
#include <QLineEdit>
#include <QPushButton>
#include <QVBoxLayout>
#include <QTextEdit>
#include <QTextCodec>
#include <QLabel>
#include <QScrollArea>
#include <QPlainTextEdit>
#include <QThread>
#include <QtNetwork/QNetworkAccessManager>
//#include <string>
//#include "staticfunctions.h"
#include "parcerslauthor.h"
#include "authortreelist.h"

WebBrowser::WebBrowser(QWidget *parent) : QWidget(parent)
{
    //
    downloading=false;
    //
//    pSa=new QScrollArea();
    pHtml=new QNetworkAccessManager(this);
    ptxtUrl=new QLineEdit();
    pbtnUrl=new QPushButton("Go");
    ptxtHtml=new QPlainTextEdit();
    pbtnFile=new QPushButton("File");
    pbtnFile2=new QPushButton("read");
    //
    connect(pbtnUrl,SIGNAL(clicked()),SLOT(btnUrlClick()));
    connect(pbtnFile,SIGNAL(clicked()),SLOT(btnFileClick()));
    connect(pbtnFile2,SIGNAL(clicked()),SLOT(btnFile2Click()));
    //
    TV=new QTreeView(this);
    //
    ptxtUrl->setText("http://samlib.ru");
    //
//    pSa->setWidget(ptxtHtml);
    //
    QHBoxLayout* phbLayout=new QHBoxLayout;
    phbLayout->addWidget(ptxtUrl);
    phbLayout->addWidget(pbtnUrl);
    //
    QVBoxLayout* pvbLayout=new QVBoxLayout;
    pvbLayout->addLayout(phbLayout);
    pvbLayout->addWidget(ptxtHtml);
    pvbLayout->addWidget(pbtnFile);
    pvbLayout->addWidget(pbtnFile2);
    pvbLayout->addWidget(TV);
    //
    this->setLayout(pvbLayout);
    //
    //TV->setModel();
    TV->hide();
    //
}

WebBrowser::~WebBrowser()
{
    delete(TVModel);
    delete(T);
}

QString WebBrowser::getHTML()
{
    return ptxtHtml->toPlainText();
}

QByteArray WebBrowser::FindCharset(QByteArray &content)
{
//    std::string Cont=StaticFunctions::FindCharset(content.toStdString());
//    return QByteArray().fromStdString(Cont);
    return QByteArray{};
}

void WebBrowser::slotFinished()
{
    ParcerSL::ParcerSLAuthor* reply=qobject_cast<ParcerSL::ParcerSLAuthor *>(sender());
    if(reply==nullptr)return;
    if(reply->parceStatus())
        t.append(reply->getResult());
    //
    std::sort(t.begin(),t.end());
    QThread *tread;
    tread=reply->thread();
    reply->deleteLater();
    tread->quit();
//    if(!tread->wait(3000)){
//        tread->terminate();
//        tread->wait();
//    }
//    tread->deleteLater();
    //
//    return;
//    //
//    QNetworkReply* reply=qobject_cast<QNetworkReply *>(sender());
//    //
//    if(reply->error()!=QNetworkReply::NoError){
//        ptxtHtml->setPlainText(reply->errorString());
//        reply->deleteLater();
//        downloading=false;
//        return;
//    }
//    //
//    QByteArray content= reply->readAll();
//    //=
//    QByteArray t=FindCharset(content);
//    //
//    std::string Cont=StaticFunctions::FindCharset(content.toStdString());
//    QTextCodec *codec = QTextCodec::codecForName(QByteArray().fromStdString(Cont));
////    QTextCodec *codec = QTextCodec::codecForName("Windows-1251");
//    //
//    QString st=codec->toUnicode(content.data());
//    //
    ptxtHtml->setPlainText("site download");
//    //
//    reply->deleteLater();
//    //
//    emit sendHTML(getHTML());
//    //
//    QList<ContentElement> q=StaticFunctions::ParceContents(st,"http://samlib.ru/s/sedrik/");
//    //
//    downloading=false;
    T=new AuthorTreeList();
    T->init(t.cbegin(),t.cend());
//    int qqq=T.ElemCount();
    //
    TVModel=new AuthorTreeModel(T,this);
    //
    TV->setModel(TVModel);
    TV->show();
    //
}

void WebBrowser::btnUrlClick()
{
    //
    ParcerSL::ParcerSLAuthor* t=new ParcerSL::ParcerSLAuthor();
    t->setHTML(ptxtUrl->text());
    QThread* tread=new QThread();
    connect(tread,&QThread::started,t,&ParcerSL::ParcerSLAuthor::Start);
    connect(tread, &QThread::finished, tread, &QThread::deleteLater);
    connect(t,SIGNAL(finished(bool)),this,SLOT(slotFinished()));
    t->moveToThread(tread);
    tread->start();
//    return;
//    //
//    if(downloading)return;
//    //
//    QUrl url(ptxtUrl->text());
//    //
//    QNetworkRequest request(url);
//    //
//    QNetworkReply* reply=pHtml->get(request);
//    connect(reply,SIGNAL(finished()),this,SLOT(slotFinished()));
//    //
//    downloading=true;
    //
//    reply->deleteLater();
    //
}

void WebBrowser::btnFileClick()
{
    //
//    QFile f1;
//    f1.setFileName("text.txt");
//    if(!f1.exists())f1.open(QIODevice::Text|QIODevice::ReadWrite);
//    else f1.open(QIODevice::Text|QIODevice::WriteOnly);
//    //
//    QTextStream ts(&f1);
//    ts.setCodec("UTF-8");
//    ts<<ptxtHtml->toPlainText().toUtf8();
//    //
//    f1.close();
//    //
//    if(f1.open(QIODevice::Text|QIODevice::ReadOnly))
//    {
//        QTextStream ts2(&f1);
//        ts2.setCodec("UTF-8");
//        QString tt;
//        while(!ts2.atEnd()){
//            ts2>>tt;
//        }
//    }
    QFile f1;
    f1.setFileName("text.txt");
    if(!f1.open(QIODevice::Text|QIODevice::WriteOnly))return;
    //
    QTextStream ts(&f1);
    for(auto p:t){
        ts<<p;
    }
    //
    f1.close();
    //
}

void WebBrowser::btnFile2Click()
{
    //
    QFile f1;
    f1.setFileName("text.txt");
    if(!f1.open(QIODevice::Text|QIODevice::ReadOnly))return;
    //
    t.clear();
    //
    QTextStream ts(&f1);
    ContentElement elem;
    while(!ts.atEnd()){
        ts>>(elem);
        t.append(elem);
    }
    //
    f1.close();
    //
}
