#ifndef WEBVIEW_H
#define WEBVIEW_H

#include <QWidget>

class QLineEdit;
class QWebEngineView;
class QPushButton;
class QProgressBar;

class WebView : public QWidget
{
    Q_OBJECT
public:
    explicit WebView(QWidget *parent = nullptr);
    void showHTML(QString url);

private:
    QLineEdit* ptxtUrl;
    QWebEngineView* pWebView;
    QPushButton* pbtnGo;
    QProgressBar* pprbHtml;

    bool loading;
signals:

public slots:
    void slotGo();
    void slotHTMLLoad(bool);

};

#endif // WEBVIEW_H
