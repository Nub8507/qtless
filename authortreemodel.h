#ifndef AUTHORTREEMODEL_H
#define AUTHORTREEMODEL_H

#include "authortreelist.h"

#include <QAbstractItemModel>
#include <QVariant>

class AuthorTreeModel: public QAbstractItemModel
{
    Q_OBJECT

public:
    explicit AuthorTreeModel(AuthorTreeList* data, QObject *parent = nullptr);
    ~AuthorTreeModel();

    Q_INVOKABLE virtual QVariant data(const QModelIndex &index, int role) const;
    Q_INVOKABLE virtual QVariant dataColumn(const QModelIndex &index, int column) const;
    virtual Qt::ItemFlags flags(const QModelIndex &index) const;
    virtual QHash<int, QByteArray> roleNames() const;
    QVariant headerData(int section, Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const;
    virtual QModelIndex index(int row, int column,
                      const QModelIndex &parent = QModelIndex()) const;
	virtual QModelIndex parent(const QModelIndex &index) const;
    virtual int rowCount(const QModelIndex &parent = QModelIndex()) const;
    virtual int columnCount(const QModelIndex &parent = QModelIndex()) const;
	virtual bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex());


    enum Roles {
        AuthorRole = Qt::UserRole + 1,
        AnotationRole
    };

	AuthorTreeList* add(const ContentElement &data);

	void add(const AuthorTreeList::NodeItem &data);

	void setTextUpdated(AuthorTreeList* data,const QDate NewDate);

	void clear();

	void changeGroupUserCheckStatus(AuthorTreeList* data,const QDate NewDate);

	QModelIndex findIndexData(AuthorTreeList* data);

private:

    AuthorTreeList *rootItem;

	void setChildUserCheckStatus(AuthorTreeList* Item,const QDate NewDate);
	void setParentUserCheckStatus(AuthorTreeList* Item,const QDate NewDate);
	void setParentTextUpdateStatus(AuthorTreeList* Item,const QDate NewDate);
};

#endif // AUTHORTREEMODEL_H
