#include "widget.h"
#include "webbrowser.h"
#include "regexttest.h"
#include "interface.h"
#include "qmlparce.h"

#include <QQmlApplicationEngine>
#include <QQmlComponent>
#include <QQmlContext>
#include <QQuickWindow>
#include <QtWidgets>
#include <QVariant>

int main(int argc, char *argv[])
{
//        QApplication a(argc, argv);

    //    RegExtTest e;
    //    e.show();

        //    Widget* w=new Widget(e);
        //    w->show();

    //    WebBrowser* b=new WebBrowser();
    //    b->show();
//        WebBrowser b;
//        b.show();

//    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QMLParce PP;

    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    QQuickWindow* window=dynamic_cast<QQuickWindow*>(engine.rootObjects()[0]);
    window->setProperty("treemodel",5);
//    //
    /* Загружаем объект в контекст для установки соединения,
     * а также определяем имя, по которому будет происходить соединение
     * */

    QObject::connect(window,SIGNAL(startParce(QString)),&PP,SLOT(startParce(QString)));
    QObject::connect(&PP,SIGNAL(TTSetVisible(QVariant)),window,SLOT(setTreeModel(QVariant)));

    //    QObject* ttt=engine.rootObjects()[0];
//    QObject* btn=ttt->findChild<QObject*>("button1",Qt::FindChildrenRecursively);
//    Interface* i=new Interface(ttt);
//    QMetaObject::Connection t;
//    if(btn)
//        t=QObject::connect(btn,SIGNAL(btnClick(QString)),&PP,SLOT(btnClick(QString)));


    return app.exec();

//       QObject::connect(b,SIGNAL(sendHTML(QString)),w,SLOT(t(QString)));

//    return a.exec();
}
