#pragma once
#ifndef STATICFUNCTIONS_H
#define STATICFUNCTIONS_H

//#include <stdlib.h>
#include <QList>
#include <string>
#include "contentelement.h"

class QString;
class QStringList;

namespace StaticFunctions{

    struct Range{
        int StartPos=0;
        int EndPos=0;
        int lenght(){return EndPos-StartPos;}
        void swap(int offset=0){StartPos=EndPos+offset;}
    };

    std::string FindCharset(std::string str);

    QList<ContentElement> ParceContents(QString,QString);

    void RemoveTags(QString&);
}

#endif // STATICFUNCTIONS_H
