#include "saveload.h"

#include "settings.h"

#include <QDir>
#include <QFile>
#include <QStandardPaths>
#include <QTextStream>


QList<QString> SaveLoad::LoadAuthorsFromFile(QString file,bool AppendPath)
{
	//
	QList<QString> Authors{};
	//
	QFile f1;
	if(AppendPath){
		auto path = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
		auto fileName= path + "/"+file;
		f1.setFileName(fileName);
	}
	else
		f1.setFileName(file);
	//
	if(!f1.open(QIODevice::Text|QIODevice::ReadOnly))return Authors;
	//
	Authors.clear();
	//
	QTextStream ts(&f1);
	QString t;
	while(ts.readLineInto(&t))
		Authors.append(t.trimmed());
	//
	f1.close();
	//
	return Authors;
	//
}

QVector<ContentElement> SaveLoad::LoadSavedListFromFile(QString file,bool AppendPath)
{
	//
	QFile f1;
	if(AppendPath){
		auto path = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
		auto fileName= path + "/"+file;
		f1.setFileName(fileName);
	}
	else
		f1.setFileName(file);
	//
    if(!f1.open(QIODevice::Text|QIODevice::ReadOnly))return QVector<ContentElement>{};
	//
    QVector<ContentElement> list;
	//
	QTextStream ts(&f1);
	while(!ts.atEnd()){
        ContentElement elem;
		ts>>(elem);
        if(elem.HTMLContentAdress.trimmed()!=""||
                elem.HTMLAuthorAdress.trimmed()!=""||
                elem.HTMLGroupAdress.trimmed()!=""||
                elem.Group.trimmed()!="")
            list.append(elem);
	}	//
	f1.close();
	//
	return list;
	//
}

QList<QString> SaveLoad::LoadAuthors()
{
	//
	switch (Settings::getSaveDir()) {
	case FILE:
		return LoadAuthorsFromFile();
	case SERVER:
		return 	QList<QString>();
	default:
		return 	QList<QString>();
	}
	//
}

QVector<ContentElement> SaveLoad::LoadSavedList()
{
	//
	switch (Settings::getSaveDir()) {
	case FILE:
        return LoadSavedListFromFile();
    case SERVER:
        return QVector<ContentElement>{};
	default:
        return QVector<ContentElement>{};
    }
	//
}

void SaveLoad::SaveAuthor(const QVector<QString>& Authors)
{
	//
    switch (Settings::getSaveDir()) {
	case FILE:
		return SaveAuthorToFile(Authors);
	case SERVER:
		return;
	default:
		return;
	}
		//
	}

void SaveLoad::SaveTextsList(const QVector<ContentElement> &list)
	{
		//
        switch (Settings::getSaveDir()) {
		case FILE:
			return SaveTextsListToFile(list);
		case SERVER:
			return;
		default:
			return;
		}
		//
	}

void SaveLoad::SaveAuthorToFile(const QVector<QString>& Authors,QString file,bool AppendPath)
{
	//
	QFile f1;
	if(AppendPath){
		auto path = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
		auto fileName= path + "/"+file;
		QDir dir(path);
		if (!dir.exists()) {
			dir.mkpath(".");
		}
		f1.setFileName(fileName);
	}
	else
		f1.setFileName(file);
	//
	if(!f1.open(QIODevice::Text|QIODevice::WriteOnly))return;
	//
	QTextStream ts(&f1);
	for(auto p:Authors){
		ts<<p<<endl;
	}
	//
	f1.close();
	//
}

void SaveLoad::SaveTextsListToFile(const QVector<ContentElement> &list,QString file,bool AppendPath)
{
	//
//    std::sort(list.begin(),list.end());
    //s
	QFile f1;
	if(AppendPath){
		auto path = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
		auto fileName= path + "/"+file;
		QDir dir(path);
		if (!dir.exists()) {
			dir.mkpath(".");
		}
		f1.setFileName(fileName);
	}
	else
		f1.setFileName(file);
	//
	if(!f1.open(QIODevice::Text|QIODevice::WriteOnly))return;
	//
	QTextStream ts(&f1);
	for(auto p:list){
        if(p.HTMLContentAdress.trimmed()!=""||
                p.HTMLAuthorAdress.trimmed()!=""||
                p.HTMLGroupAdress.trimmed()!=""||
                p.Group.trimmed()!="")
			ts<<p;
	}
	//
	f1.close();
	//
}
