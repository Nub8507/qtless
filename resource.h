#ifndef RESOURCE_H
#define RESOURCE_H

#include "authortreelist.h"
#include "authortreemodel.h"
#include "saveload.h"

#include <QObject>

class Resource : public QObject
{
	Q_OBJECT
public:
	explicit Resource(QObject *parent = nullptr);

	AuthorTreeModel* getTLModel();					//получаем ссылку на модель дерева для GUI

	void setAuthors(const QList<QString> &value);	//заполнить спиисок авторов
	QList<QString> getAuthors() const;				//получаем список авторов

signals:
	void ModelUpdated();							//вызывается при обновлении модели
	void FinishAuthorChange();						//вызывается при изменении списка авторв
	void FinishDataLoading();						//вызывается при окончании загрузки произведений
    void DataListReadyToSave(const QVector<ContentElement>& list);	//вызывается в ответ на SaveList, возвращает произведения в виде списка
	void AuthorsListReadyToSave(const QVector<QString>& list);		//вызывается в ответ на SaveAuthors,возвращает авторов в виде списка
	void SendTrayMessage(const QString title,const QVector<QString> txtList);	//вызывается для отправки сообщений пользователю
    void ParceFinish(QVector<ContentElement>& list);	//вызывается после расшифровки произведений, возвращает список расшифровки
	void ElementChanged(const ContentElement& elem);//вызывается при изменении элемента, возвращает элемент
	void ElementDeleted(const ContentElement& elem);//вызывается при удалении элемента, возвращает копию элемента
	void AllContentData(QList<QString> Authors,QVector<ContentElement> list);//вызывается в ответ на getAllContentData, возвращает списки авторов и произведений

public slots:
	void addAuthor(QString Author);							//добавление автора
	void startParce(QString txtHTML);						//начать расшифровку страницы
    void initList(QVector<ContentElement> &list);           //начальное заполнение дерева из списка
    void changeList(QVector<ContentElement> &list);         //нужно сравнить и обновить дерево произведений
	void saveList();										//нужно сохранить спиcок произведений
	void saveAuthors();										//нужно сохранить список авторов
	void saveAll();											//нужно сохранить список авторов и произведений
	void userCheckedUpdate(ContentElement elem);			//пользователь посмотрел произведение, нужно пометить о просмотре
	void userDelAuthor(ContentElement elem);				//пользователь удалил автора
	void checkDeleted();									//проверяет и очищает дерево на наличие удалннных элементов
	void setDeleted(QString Author);						//устанавливает пометку удаления всем произведениям автора
	void getAllContentData();								//запрос всех данных
	void elementChange(ContentElement elem);				//изменился элемент
	void elementDelete(ContentElement elem);				//удалили элемент
	void initAllData(QList<QString> Authors,QVector<ContentElement> list); //заполнение списка авторов и произведений

private slots:
	void slotParceFinished();			//вызывается при окончании расшифровки HTML-страницы
	void deleteAuthor(QString Author);	//удаление автора
	void dataChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight, const QVector<int> &roles = QVector<int>()); //вызывается при изменении модели

private:

	AuthorTreeList TreeList;					//дерево произведений
	QList<QString> Authors{};					//список авторов
	AuthorTreeModel TLModel{&TreeList,this};	//модель дерева произведений

    void compare(QVector<ContentElement> &list);	//функция сравения и изменения произведений в дереве
    QVector<ContentElement> treeToList(AuthorTreeList* parent);	//сохраняет дерево произведений в список
	QString fillTextNotify(const ContentElement &elem);	//заполняет сообщение пользователю
	void checkDeleted(AuthorTreeList* parent);			//проверяет и удаляет дочерние элементы дерева, помеченные на удаление
	void setDeleted(AuthorTreeList* index);		//помечает элемент и дочерние элементы дерева на удаление

};

#endif // RESOURCE_H
