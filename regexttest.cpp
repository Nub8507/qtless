#include "regexttest.h"
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLineEdit>
#include <QLabel>
#include <QPushButton>
#include <regex>
#include <string>


RegExtTest::RegExtTest(QWidget *parent) : QWidget(parent)
{
    ptxtRegExp=new QLineEdit();
    ptxtExpr=new QLineEdit();
    plblResult=new QLabel();
    pbtnExec=new QPushButton("Выполнить");
    //
    connect(pbtnExec,SIGNAL(clicked()),SLOT(btnClick()));
    //
    QHBoxLayout* phbLayout=new QHBoxLayout();
    phbLayout->addWidget(ptxtRegExp);
    phbLayout->addWidget(pbtnExec);
    //
    QVBoxLayout* pvbLayout=new QVBoxLayout();
    pvbLayout->addLayout(phbLayout);
    pvbLayout->addWidget(ptxtExpr);
    pvbLayout->addWidget(plblResult);
    //
    this->setLayout(pvbLayout);
}

//void RegExtTest::btnClick()
//{
//    std::regex reg(ptxtRegExp->text().toStdString());
//    std::string S=ptxtExpr->text().toStdString();
//    //
//    std::string t;
//    std::smatch m;
//    bool result=regex_search(S,m,reg);
//    if(result){
//        //
//        for(auto pos=m.cbegin();pos!=m.cend();++pos){
//            t+=*pos;
//            t+="\n";
//        }
//        //
//    }
//    plblResult->setText(QString().fromStdString(t));
//    //
//}

void RegExtTest::btnClick()
{
    QRegExp reg(ptxtRegExp->text());
    reg.setMinimal(true);
    reg.setCaseSensitivity(Qt::CaseInsensitive);
    QString S=ptxtExpr->text();
    QStringList list;
    int pos = 0;

    while ((pos = reg.indexIn(S, pos)) != -1) {
        list << reg.cap(1);
        pos += reg.matchedLength()==0?1:reg.matchedLength()-reg.cap(2).length();
    }

}
