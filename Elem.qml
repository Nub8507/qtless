import QtQuick 2.11
import QtQuick.Controls 2.11
//import Qt.labs.platform 1.1
Rectangle {
    id: rectangle

    property string txtGroup: ""
    property string txtContent: ""
    property string txtHTML: ""
    property bool txtUpdated: false
    property bool hasChaild: false
    property bool topLevel: false

//    property variant list

    signal expand(var index)

    width: parent.width
    height: 150
    radius: 2
    border.width: 2
    border.color: "#9e1d1d"
    anchors.left: parent.left
    anchors.leftMargin: 8

    Text {
        id: textAuthor
        x: 3
        y: 3
        text: txtGroup
        clip: true
        anchors.bottom: parent.top
        anchors.bottomMargin: -26
        anchors.right: {
            if(topLevel)ibuttonDel.left
            else ibuttonExp.left
        }
        anchors.rightMargin: 6
        anchors.left: parent.left
        anchors.leftMargin: 3
        anchors.top: parent.top
        anchors.topMargin: 3
        font.bold: true
        wrapMode: Text.NoWrap
        font.pixelSize: 18
    }

    ImageButton{
        id: ibuttonUpd
        x: 329
        y: 3
        width: 23
        height: 23
        anchors.top: parent.top
        anchors.topMargin: 3
        anchors.right: parent.right
        anchors.rightMargin: 8

        btnImagePressed: {
            if(txtUpdated)
                "qrc:/Image/icons8-idea-96.png";
            else
                "qrc:/Image/icons8-idea-off-96.png";
        }
        btnImageReleased: {
            if(txtUpdated)
                "qrc:/Image/icons8-idea-96.png";
            else
                "qrc:/Image/icons8-idea-off-96.png";
            }

        onBtnClicked: {
            updateChecked(styleData.index)
        }

    }

    ImageButton{
       id: ibuttonExp
       x: 300
       y: 11
       width: 23
       height: 23
       anchors.rightMargin: 6
       anchors.topMargin: 3
       anchors.right: ibuttonUpd.left
       anchors.top: parent.top
       visible: hasChaild

       btnImagePressed: {
           if(styleData.isExpanded)
               "qrc:/Image/icons8-collapse-96.png";
           else
               "qrc:/Image/icons8-expand-96.png";
       }
       btnImageReleased: {
           if(styleData.isExpanded)
               "qrc:/Image/icons8-collapse-96.png";
           else
               "qrc:/Image/icons8-expand-96.png";
           }

       onBtnClicked: {
           expand(styleData.index)
       }

    }

    ImageButton {
        id: ibuttonDel
        x: 271
        y: 8
        width: 23
        height: 23
        btnImagePressed:"qrc:/Image/icons8-trash-96.png"
        btnImageReleased:"qrc:/Image/icons8-trash-96.png"
        anchors.right: ibuttonExp.left
        anchors.rightMargin: 6
        anchors.top: parent.top
        visible: topLevel
        anchors.topMargin: 3

        onBtnClicked: {
            dialogDel.open()
        }

    }

    Text {
        id: textContent
        x: 6
        y: 32
        text: txtContent
        clip: true
        anchors.rightMargin: 6
        anchors.leftMargin: 6
        anchors.bottomMargin: 6
        anchors.top: textAuthor.bottom
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.topMargin: 6
        wrapMode: Text.WordWrap
        font.pixelSize: 13

        MouseArea {
            id: mouseArea
            anchors.fill: parent
            onDoubleClicked:{
                //openText(styleData.index)
                Qt.openUrlExternally(txtHTML)
                updateChecked(styleData.index)
            }
        }
    }

    Dialog{
        id: dialogDel
        modal: true
//        modality: Qt.WindowModal
        title: qsTr("Delete?")
        standardButtons: Dialog.Yes | Dialog.No
//        MessageDialog: MessageDialog.Yes | MessageDialog.No

        onAccepted: del(styleData.index)
//        onRejected:
    }
}
