#include "webview.h"
#include "QLineEdit"
#include "QPushButton"
#include <QtWebEngineWidgets/QtWebEngineWidgets>

WebView::WebView(QWidget *parent) : QWidget(parent)
{
    ptxtUrl=new QLineEdit();
    pbtnGo=new QPushButton("Go");
    pWebView=new QWebEngineView();
    pprbHtml=new QProgressBar();


    connect(pbtnGo,SIGNAL(clicked()),SLOT(slotGo()));
    connect(pWebView,SIGNAL(loadProgress(int)),pprbHtml,SLOT(setValue(int)));
    connect(pWebView,SIGNAL(loadFinished(bool)),SLOT(slotHTMLLoad(bool)));

    pWebView->setEnabled(false);
    ptxtUrl->setText("http://samlib.ru");
    loading=false;

    QHBoxLayout* phbLayout=new QHBoxLayout();
    phbLayout->addWidget(ptxtUrl);
    phbLayout->addWidget(pbtnGo);

    QVBoxLayout* pvbLayout=new QVBoxLayout();
    pvbLayout->addLayout(phbLayout);
 //   pvbLayout->addStretch(1);
    pvbLayout->addWidget(pWebView);
    pvbLayout->addWidget(pprbHtml);
    //
    this->setLayout(pvbLayout);
}

void WebView::showHTML(QString url)
{
    QString T=url;
    if(!T.startsWith("ftp://")
            &&!T.startsWith("http://")
            &&!T.startsWith("https://")
            &&!T.startsWith("gopher://")){
       T="http://"+T;
    }
    //
    pWebView->load(QUrl(T));
    loading=true;
    //
}

void WebView::slotGo()
{
    //
    if(loading) return;
    //
    showHTML(ptxtUrl->text());
    //
}

void WebView::slotHTMLLoad(bool bOK)
{
    if(!bOK){
        pWebView->setHtml("<CENTER> При загрузке страницы произошла ошибка. </CENTER>");
    }
    //
    loading=false;
}

