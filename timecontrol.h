#ifndef TIMECONTROL_H
#define TIMECONTROL_H

#include "resource.h"

#include <QObject>
#include <QSharedPointer>
#include <QTimer>

class timeControl : public QObject
{
	Q_OBJECT
public:
	explicit timeControl(QObject *parent = nullptr);

	void setResource(Resource *value);					//получаем ссылку на данные

signals:
	void StartUpdate(QString HTMLAuthor);				//нужно начать обновления данных автора
	void SaveList();									//нужно сохранить данные
#ifdef QT_DEBUG
	void DebugMessage(const QString title,const QList<QString> txtListS);	//отправка отладочного всплывающего сообщения
#endif

public slots:
	void startWork(int interval);					//начинант работу таймеров
	void stopWork();								//остановка работы таймеров
	void updateAll();								//необходимо обновить всех авторов
	void parceFinish();								//вызывается после расшифровки данных автора

private slots:
	void needUpdateAuthor();						//срабатывение таймера обновления автора
	void needSaveData();							//срабатывение таймера сохранения
#ifdef QT_DEBUG
	void needDebugMessageSend();					//срабатывение таймера отладочного сообщения
#endif

private:
	QTimer timerListUpdate;							//таймер обновлений
	QTimer timerSaveData;							//таймер сохранений
#ifdef QT_DEBUG
	QTimer timerDebugMessage;						//таймер отладки

#endif
	Resource* Res;										//ссылка на данные
	QString lastAuhor;									//последний обновленный автор

	bool needAllUpdate;									//нужно обновить всех авторов
};

#endif // TIMECONTROL_H
