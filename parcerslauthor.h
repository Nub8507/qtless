#ifndef PARCERSLAUTHOR_H
#define PARCERSLAUTHOR_H

#include <QObject>

#include <contentelement.h>
class QString;
class QStringList;
class QNetworkAccessManager;
class QNetworkReply;

namespace ParcerSL{

/*Тип html страницы
 * Main - начальная страница автора
 * Group - группы на отдельных страницах
 * Text - текст произведения
 * Unknown - неизвестный тип страницы или расшифровка не завершена
 * ErrorHTML - ошибка получения html страницы
 * ErrorParce - ошибка расшифровки
*/
enum HTMLPageType{Main=0,Group,Text,Unknown,ErrorHTML,ErrorParce};

class ParcerSLAuthor : public QObject
{
    Q_OBJECT
private:

    //диапазон подстроки псле поиска
    struct Range{
        int StartPos=0;
        int EndPos=0;
        int lenght() {return EndPos-StartPos;}
        void swap(int offset=0) {StartPos=EndPos+offset;}
    };

    //хранит html ссылку для расшифровки
    QString HTMLSource="";

    //родительский элемент расшифровки
    ContentElement Parent{};

    //результат расшифровки
    QList<ContentElement> ParceResult;

    //статус расшифровки true-выполнена, false-нет
    bool status=false;

    //Тип html страницы
    HTMLPageType PageType=Unknown;

    //доступ к сети
    QNetworkAccessManager* pHtml=nullptr;
    bool downloading=false;

    //Количество запущенных процессов расшифровки
    int NumChildren=1;

    //Удалить <> теги из строки
    void RemoveTags(QString& source);

    //поиск кодировки в html тексте, если не найдена возвращает Windows-1251
    QByteArray FindCharset(QByteArray& source);

    //разбивает html текст на подстроки. конец строки по символу \n
    inline QStringList StrToList(QString& source){return source.split("\n");}

    //поиск именми автора в html-тексте
    QString FindAuthor(const QStringList& List);

    //поиск диапазона строк с произведениями
    Range FindContentList(const QStringList& List);

    //расшифровка аннотации к произведению
    QString ParceAnotation(QString source);

    //расшифровка строки описания произведения, результат добавляется в DataList
    void ParceBookString(QString& source,const ContentElement& pelem);

    //расшифровка строк группы, результат добавляется в DataList
    ContentElement ParceGroupString(QString s1,QString s2, const ContentElement& pelem);

    //расшифровка списка произведеий. определение групп и отдельных произведениц
    bool ParceMainGroupPage(const QStringList& List,Range pos,const ContentElement& pelem);

    //Расшифровать полученную HTML страницу
    bool ParceContent(QString& source);

    //определяем тип полученной страницы
    HTMLPageType FindPageType(const QStringList& List);

    //Расшифруем странице с текстом произведения
    bool ParceTextPage(const QStringList& List,ContentElement& elem);

    //поиск диапазона строк с описанием произведения
    Range FindTextList(const QStringList& List);

public:
    explicit ParcerSLAuthor(QObject *parent = nullptr);
    explicit ParcerSLAuthor(QString HTML,QObject *parent = nullptr);
    ParcerSLAuthor(ContentElement& p,QString HTML,QObject *parent = nullptr);


    //получает html-ссылку
    inline QString getHTML() const {return HTMLSource;}

    //устанавливает новую ссылку для расшифровки
    inline void setHTML(QString HTML) {HTMLSource=HTML;status=false;PageType=Unknown;}

    //возвращает статус расшифровки
    inline bool parceStatus() const {return status;}

    //Возвращает результат расшифровки
    QVector<ContentElement> getResult() {return ParceResult.toVector();}

    //тип расшифровываемой страницы
    HTMLPageType getPageType() const {return PageType;}

signals:
    //вызывается при завершении расшифровки
    void finished(bool);

public slots:

    //Начать расшифровку
    void Start();


private slots:
    //завершено получение данных с сайта
    void slotHTMLFinish();
    //завершена расшифровка дочерней страницы
    void ChildFinish(bool);
};
}
#endif // PARCERSLAUTHOR_H
