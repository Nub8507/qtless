#ifndef CONTENTELEMENT_H
#define CONTENTELEMENT_H

#include <QString>
#include <QDate>
#include <QTextStream>
#include <QList>

#ifdef DEBUG_MODE

	#include<qlogging.h>

#endif

struct ContentElement
{
public:
    QString Author;
    QString HTMLAuthorAdress;
    QString Group;
    QString HTMLGroupAdress;
    QString Content;
    QString HTMLContentAdress;
    QString Anotation;
//    int ID;
//    int ParentID;
//    const int offsetID=1000;
//    QDate CurrUpdate;
	QDate LastUpdate;						//Дата последнего обновления элемента
	QDate UserCheck;						//Дата просмотра пользователем
//    int level;

    ContentElement(){
        Author="";
        HTMLAuthorAdress="";
        Group="";
        HTMLGroupAdress="";
        Content="";
        HTMLContentAdress="";
        Anotation="";
        LastUpdate=QDate(1900,01,01);
        UserCheck=QDate(1900,01,01);
    }

    bool operator<(const ContentElement& elem)const{
        if(this->Author<elem.Author) return true;
        else if (this->Author==elem.Author){
            if(this->Group<elem.Group)return true;
            else if (this->Group==elem.Group){
                if(this->Content<elem.Content)return true;
                else if(this->Content==elem.Content){
                    if(this->Anotation<elem.Anotation)return true;
                }
            }
        }
        return false;
    }
    bool operator==(const ContentElement& elem)const{
        if(this->Author==elem.Author&&
                this->Group==elem.Group&&
                this->Content==elem.Content&&
                this->Anotation==elem.Anotation&&
				//this->CurrUpdate==elem.CurrUpdate) return true;
				this->LastUpdate==elem.LastUpdate) return true;
        //
        return false;
    }
    bool operator<=(const ContentElement& elem)const {
        if(*this==elem||*this<elem)return true;
        return false;        
    }
    bool equal(const ContentElement& elem,int deep=3)const{
		#ifdef DEBUG_MODE
			qDebug() << this->HTMLAuthorAdress<<endl;
			auto d2=this->HTMLAuthorAdress;
			auto d3=this->HTMLAuthorAdress;
		#endif

        Q_UNUSED(deep);

        if(elem.HTMLContentAdress!=""){
            if(this->HTMLAuthorAdress==elem.HTMLAuthorAdress&&
                    this->HTMLGroupAdress==elem.HTMLGroupAdress&&
                    this->Group==elem.Group&&
                    this->HTMLContentAdress==elem.HTMLContentAdress) return true;
        }else if (elem.Group!=""||elem.HTMLGroupAdress!=""){
            if(this->HTMLAuthorAdress==elem.HTMLAuthorAdress&&
                    this->HTMLGroupAdress==elem.HTMLGroupAdress&&
                    this->Group==elem.Group) return true;
        }else if (elem.HTMLAuthorAdress!=""){
            if(this->HTMLAuthorAdress==elem.HTMLAuthorAdress) return true;
        }
        return false;
        //
        switch (deep) {
            case 1:
            if(this->HTMLAuthorAdress==elem.HTMLAuthorAdress) return true;
            break;
            case 2:
            if(this->HTMLAuthorAdress==elem.HTMLAuthorAdress&&
                    this->HTMLGroupAdress==elem.HTMLGroupAdress&&
                    this->Group==elem.Group) return true;
            break;
            case 3:
            if(this->HTMLAuthorAdress==elem.HTMLAuthorAdress&&
                    this->HTMLGroupAdress==elem.HTMLGroupAdress&&
                    this->Group==elem.Group&&
                    this->HTMLContentAdress==elem.HTMLContentAdress) return true;
            break;
            default:
            return false;
        }
        //
        return false;
    }
//    ContentElement& operator=(const ContentElement& elem){
//        this->Author=elem.Author;
//        this->HTMLAuthorAdress=elem.HTMLAuthorAdress;
//        this->Group=elem.Group;
//        this->HTMLGroupAdress=elem.HTMLGroupAdress;
//        this->Content=elem.Content;
//        this->HTMLContentAdress=elem.HTMLContentAdress;
//        this->Anotation=elem.Anotation;
//        this->ParentID=elem.ID;
//        this->ID=elem.ID+offsetID;
//        this->CurrUpdate=elem.CurrUpdate;
//        return *this;
//    }


    inline bool isEmpty(){
            return this->Author.isEmpty()&&this->HTMLAuthorAdress.isEmpty();
        }

    friend QTextStream& operator<<(QTextStream& out,const ContentElement& elem)
    {
        QTextCodec* tt=out.codec();
        out.setCodec("UTF-8");
        //
        out<<elem.Author<<" }}"<<endl;
        out<<elem.HTMLAuthorAdress<<" }}"<<endl;
        out<<elem.Group<<" }}"<<endl;
        out<<elem.HTMLGroupAdress<<" }}"<<endl;
        out<<elem.Content<<" }}"<<endl;
        out<<elem.HTMLContentAdress<<" }}"<<endl;
        out<<elem.Anotation<<" }}"<<endl;
//        out<<elem.CurrUpdate.toString("dd/MM/yyyy")<<" }}"<<endl;
//        out<<elem.ID<<" }}"<<endl;
//        out<<elem.ParentID<<" }}"<<endl;
		out<<elem.LastUpdate.toString("dd/MM/yyyy")<<" }}"<<endl;
		out<<elem.UserCheck.toString("dd/MM/yyyy")<<" }}"<<endl;
//		out<<elem.level<<" }}"<<endl;
        //
        out.setCodec(tt);
        return out;
    }

    friend QTextStream& operator>>(QTextStream& in,ContentElement& elem)
    {
        QTextCodec* tt=in.codec();
        in.setCodec("UTF-8");
        //

		//
        if(!in.atEnd()){
            elem.Author=elem.readLine(in);
            elem.HTMLAuthorAdress=elem.readLine(in);
            elem.Group=elem.readLine(in);
            elem.HTMLGroupAdress=elem.readLine(in);
            elem.Content=elem.readLine(in);
            elem.HTMLContentAdress=elem.readLine(in);
            elem.Anotation=elem.readLine(in);
//			elem.CurrUpdate=QDate::fromString(elem.readLine(in),"dd/MM/yyyy");
			elem.LastUpdate=QDate::fromString(elem.readLine(in),"dd/MM/yyyy");
			elem.UserCheck=QDate::fromString(elem.readLine(in),"dd/MM/yyyy");
//            elem.ID=elem.readLine(in).toInt();
//            elem.ParentID=elem.readLine(in).toInt();
//            elem.level=elem.readLine(in).toInt();
        }
        //
        in.setCodec(tt);
        return in;
    }

	QString toString(){
		//
		QString S="";
		//
		QTextStream in(&S);
		in<<*this;
		//
		return S;
		//
	}

	void init(QString str){
		QTextStream out(&str);
		ContentElement elem;
		out>>*this;
	}


private:
    const QString readLine(QTextStream& in){
        //
        QString result="",t="";
        //
		while(!in.atEnd()&&t!="}} ")
        {
            result+=t;
            in>>t;
            t+=" ";
        }
        //
		return result.trimmed();
    }
};

#endif // CONTENTELEMENT_H
