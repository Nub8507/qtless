#ifndef ROSERVER_H
#define ROSERVER_H

#include <QObject>
#include "authortreelist.h"
#include "contentelement.h"
#include "rep_SLViewQtRemoute_source.h"


class ROServer: public SLViewQtRemouteSimpleSource
{
	Q_OBJECT
public:
	ROServer(QObject *parent = nullptr);
	~ROServer();

	virtual void r_startParce(QString txtHTML);
	virtual void r_AddAuthor(QString Author);
	virtual void r_UpdateAll();
	virtual void r_SaveAll();
	virtual void r_userCheckedUpdate(QString index);
	virtual void r_del(QString index);
	virtual void r_getAllContentData();

signals:
	void startParce(QString txtHTML);
	void AddAuthor(QString Author);
	void updateAll();
	void SaveAll();
	void userCheckedUpdate(ContentElement);
	void del(ContentElement);
	void AllContentData();

public slots:
	void initUserData(QList<QString> Authors,QList<AuthorTreeList::NodeItem> elements);
	void ElementChanged(AuthorTreeList::NodeItem elem);
	void ElementDeleted(AuthorTreeList::NodeItem elem);
};

#endif // ROSERVER_H
