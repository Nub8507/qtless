#ifndef ANDROIDAPP_H
#define ANDROIDAPP_H

#include "roreplica.h"
#include "roserver.h"
//#include <contentelement.h>

#include <QObject>

class QQmlApplicationEngine;
class QQuickWindow;

class SaveLoad;
class Interface;
class Resource;
class timeControl;
class AppServiceBridge;

class AndroidApp : public QObject
{
	Q_OBJECT
public:
	AndroidApp(int argc, char *argv[], QObject *parent = nullptr);
	~AndroidApp();

	int exec();

signals:
	void debugDialog(QVariant mess);

private slots:
	void connectionLost();
	void sendDebugMessage();
	void sendDebugMessage(QString mess);
	void initReplicaConnection();
	void sendTrayMessage(const QString title,const QList<QString> txtList);

private:
//	QGuiApplication* app1;
//	QAndroidService* app2;
	//
	QObject* app;
	QQmlApplicationEngine* engine;
	QQuickWindow* window;
	//
	SaveLoad* File;
	Interface* Gui;
	Resource* Data;

#ifdef QT_DEBUG
	Resource* Data2;
	QRemoteObjectHost* Node2;

#endif


	timeControl* Timer;
	AppServiceBridge* Bridge;
	//
	bool ThisIsService;
	int ErrorExit;
	//
	QRemoteObjectRegistryHost* regNode;
	QRemoteObjectHost* Node;
	//
	ROServer ROServerClass;
	//
	QSharedPointer<QRemoteObjectDynamicReplica> dynRepPtr;
	//
	RoReplica* ROReplicaClass;
	//
	void initServer();
	void initReplica();
	//

};

#endif // ANDROIDAPP_H
