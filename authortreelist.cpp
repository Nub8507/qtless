#include "authortreelist.h"

AuthorTreeList::AuthorTreeList()
{
    m_parentItem=nullptr;
    m_childItems.clear();
}

AuthorTreeList::AuthorTreeList(AuthorTreeList *parent)
{
    m_parentItem=parent;
    m_childItems.clear();
}

AuthorTreeList::AuthorTreeList(const ContentElement& Element,AuthorTreeList *parent)
{
    m_parentItem=parent;
    m_childItems.clear();
	m_itemData.fromContElement(Element);
}

AuthorTreeList::~AuthorTreeList()
{
    qDeleteAll(m_childItems);
	m_childItems.clear();
}


AuthorTreeList* AuthorTreeList::appendChild(const ContentElement& Element){
    AuthorTreeList* T= new AuthorTreeList{Element,this};
    this->m_childItems.append(T);
	T->setDeleted(false);
    return T;
}

AuthorTreeList* AuthorTreeList::child(int row){
    if(row<0||row>=m_childItems.size())return nullptr;
    return m_childItems[row];
}

int AuthorTreeList::childCount() const {
    return m_childItems.count();
}

int AuthorTreeList::columnCount() const
{
	return 5;
}

int AuthorTreeList::row() const{
    if (m_parentItem)
        return m_parentItem->m_childItems.indexOf(const_cast<AuthorTreeList*>(this));

    return -1;
}

AuthorTreeList::NodeItem& AuthorTreeList::data(){
    return m_itemData;
}

AuthorTreeList* AuthorTreeList::parent(){
    return m_parentItem;
}

template <typename T>
void AuthorTreeList::init(T first,T end,int deep)
{
    //
    if (first>=end)return;
    //
    auto pos=first+1;
    auto pos1=first;
    //
	auto Group=appendChild(*first);						//добавляем новый элемент в дочерние
	while (pos<end){									//пока не весь переданный список просмотрен
        //
		if(!((*pos).equal(*pos1,deep))){				//если сменился уровень группировки для элементов списка
			Group->init(pos1+1,pos,deep+1);				//добавленный элемент группы заполняем полученным диапазоном списка
            pos1=pos;
            Group=appendChild(*pos);
        }
        //
        ++pos;
        //
    }
    //
    Group->init(pos1+1,pos,deep+1);
}

int AuthorTreeList::ElemCount()
{
    int sum=m_childItems.size();
    for(auto p:m_childItems){
        sum+=p->ElemCount();
    }
    //
    return sum;
}

AuthorTreeList* AuthorTreeList::Find(const ContentElement &data)
{
    //
	AuthorTreeList *root=this->FindRoot();				//находим корневой элемент
	//
	return FindFromRoot(data,root,3);					//производим поиск от корневого элемента по полному совпадению
    //
}

AuthorTreeList *AuthorTreeList::FindParent(const ContentElement &data)
{
    //
	AuthorTreeList *root=this->FindRoot();				//находим корневой элемент
	//
    ContentElement elem=data;
    elem.HTMLContentAdress="";
    AuthorTreeList* result=FindFromRoot(elem,root);	//пробуем найти родителя среди групп
    //
	if(result!=nullptr) return result;					//если родитель найден - возврат
    //
    elem.HTMLGroupAdress="";
    elem.Group="";
    return FindFromRoot(elem,root);					//пробуем найти родителя среди авторов
	//
}

AuthorTreeList* AuthorTreeList::add(const ContentElement &data)
{
	//
	AuthorTreeList *parent=nullptr;
	AuthorTreeList *root=this->FindRoot();			//находим корневой элемент
	//
	parent=FindParent(data);						//пробуем найти родителя
	//
	if (parent==nullptr){							//если родитель не найден
		//
		return root->appendChild(data);				//добавляем элемент в корень
		//
	}
	//
	return parent->appendChild(data);				//добавляем элемент
	//
}

AuthorTreeList *AuthorTreeList::add(const AuthorTreeList::NodeItem &data)
{
	//
	AuthorTreeList * T=this->add(data.source);
	T->setLastUpdate(data.getLastUpdate());
	T->setUserCheck(data.getUserCheck());
	//
	return T;
	//
}

bool AuthorTreeList::del(AuthorTreeList *data)
{
	//
	AuthorTreeList *root=this->FindRoot();	//находим корневой элемент
	//
	if(data==root){							//если удаляем корневой элемент
		root->clear();						//очищаем дерево
		return true;
	}
	//
	AuthorTreeList *parent=data->parent();	//удаляем элемент из списка родителя
	parent->m_childItems.removeOne(data);
	//
	delete data;							//удаляем элемент
	//
	return true;
	//
}

AuthorTreeList* AuthorTreeList::clear()
{
	//
	AuthorTreeList *root=this->FindRoot();//находим корневой элемент
	//
	qDeleteAll(root->m_childItems);
	//
	return root;
	//
}

int AuthorTreeList::findRow(AuthorTreeList *data)
{
	return this->m_childItems.indexOf(data);
}

AuthorTreeList *AuthorTreeList::FindRoot()
{
	AuthorTreeList *root;
	root=this;
	//
	while (root->parent()!=nullptr)						//находим корневой элемент
		root=root->parent();
	//
	return root;
	//
}


AuthorTreeList* AuthorTreeList::FindFromRoot(const ContentElement &data, AuthorTreeList *root,int deep)
{
    //
	if(root->m_itemData.source.equal(data,deep))			//если переданный элемент искомый
		return root;										//возвращаем его
    //
	if (root->childCount()==0)								//если исходный элемент не подходит и дочерних нет - ничего не нашли
            return nullptr;
    //
    AuthorTreeList* result=nullptr;
    //
	for (auto ch:root->m_childItems)						//для всех дочерних элементов
    {
		result=ch->FindFromRoot(data,ch,deep);				//запускаем поиск
        if(result!=nullptr)
            return result;
    }
    //
	return nullptr;											//ничего не нашли
    //
}

QDate AuthorTreeList::getUserCheck() const
{
	return m_itemData.getUserCheck();
}

void AuthorTreeList::setUserCheck(const QDate &value)
{
	m_itemData.setUserCheck(value);
}

QDate AuthorTreeList::getLastUpdate() const
{
	return m_itemData.getLastUpdate();
}

void AuthorTreeList::setLastUpdate(const QDate &value)
{
	m_itemData.setLastUpdate(value);
}

QString AuthorTreeList::getHTMLAdress() const
{
	return m_itemData.HTMLAdress;
}

void AuthorTreeList::setDeleted(bool value)
{
	m_itemData.deleted = value;
}

bool AuthorTreeList::getDeleted() const
{
	return m_itemData.deleted;
}
