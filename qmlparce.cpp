#include "qmlparce.h"

#include "authortreelist.h"
#include "authortreemodel.h"
#include "parcerslauthor.h"
#include "contentelement.h"

#include <QQmlContext>
#include <QQuickWindow>
#include <QThread>


QMLParce::QMLParce(QObject *parent) : QObject(parent)
{
    T=new AuthorTreeList();
//    TVModel=new AuthorTreeModel();

}

QMLParce::~QMLParce()
{
    delete(TVModel);
    delete(T);
}

void QMLParce::startParce(QString txtHTML)
{
    //
    ParcerSL::ParcerSLAuthor* t=new ParcerSL::ParcerSLAuthor();
    t->setHTML(txtHTML);
    QThread* tread=new QThread();
    connect(tread,&QThread::started,t,&ParcerSL::ParcerSLAuthor::Start);
    connect(tread, &QThread::finished, tread, &QThread::deleteLater);
    connect(t,SIGNAL(finished(bool)),this,SLOT(slotFinished()));
    t->moveToThread(tread);
    tread->start();
    //
}

void QMLParce::slotFinished()
{
    //
    ParcerSL::ParcerSLAuthor* reply=qobject_cast<ParcerSL::ParcerSLAuthor *>(sender());
    if(reply==nullptr)return;
    if(reply->parceStatus())
        t.append(reply->getResult());
    //
    std::sort(t.begin(),t.end());
    QThread *tread;
    tread=reply->thread();
    reply->deleteLater();
    tread->quit();
    //
    //T->init(t.cbegin(),t.cend());
    for (auto p:t){
        T->add(p);
    }
//    int qqq=T.ElemCount();
    //
    TVModel=new AuthorTreeModel(T,this);
    //
    emit TTSetVisible(QVariant::fromValue(TVModel));
    //
}
