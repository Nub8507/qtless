#include "authortreelist.h"
#include "interface.h"
#include "settings.h"

Interface::Interface(QObject *parent):QObject (parent)
{
}

void Interface::updateChecked(const QVariant index)
{
	//
	auto i=index.toModelIndex();
	//
	if(i.isValid()){												//если индекс модели правильный
		auto t=static_cast<AuthorTreeList*>(i.internalPointer());
		emit UserCheckedUpdate(t->data().source);					//посылаем сигнал с данными элемента
	}
	//
}

void Interface::sendTrayMessage(const QString title, const QVector<QString> txtList)
{
	//
	QString text;
	//
	int min=std::min(4,txtList.count());					//для ограничения размера сообщения
	for(int i=0;i<min;++i)									//определяем количество обновленных произведений
		text=text+txtList.at(i)+"\n";						//и выводим на экран только первые 4
	//
	if(min<txtList.count())									//если произведений больше 4
		text=text+"...";									//добавляем в конец сообщения "..."
	else
		text=text.trimmed();
	//
	emit SendTrayMessage(QVariant(title),QVariant(text));
	//
}

void Interface::del(const QVariant index)
{
	auto i=index.toModelIndex();
	//
	if(i.isValid())											//если индекс модели правильный
		emit Del(static_cast<AuthorTreeList*>(i.internalPointer())->data().source);	//посылаем сигнал с данными элемента
	//
}

void Interface::updateAll()
{
	//
	emit UpdateAll();
	//
}

void Interface::needLoadSettings()
{
	//
	int sd=Settings::getSaveDir();
	//
	emit LoadSettings(QVariant(sd==0?true:false),QVariant(Settings::getLogin()),QVariant(Settings::getPassword()));
	//
}

void Interface::saveSettings(QVariant toF, QVariant txtLogin, QVariant txtPassword)
{
	//
	if(toF.toBool())
		Settings::setSaveDir(SaveLoad::FILE);
	else
		Settings::setSaveDir(SaveLoad::SERVER);
	//
	Settings::setLogin(txtLogin.toString());
	Settings::setPassword(txtPassword.toString());

	//
}

void Interface::startParce(QString txtHTML)
{
	emit StartParce(txtHTML);
}

//void Interface::openText(const QVariant index)
//{
//	//
//	QModelIndex i=index.toModelIndex();
//	//
//	if(i.isValid())
//		emit userOpenText(i);
//	//
//}
