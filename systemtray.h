#ifndef SYSTEMTRAY_H
#define SYSTEMTRAY_H

#include <QLabel>

class QMenu;
class QSystemTrayIcon;

class SystemTray : public QLabel
{
	Q_OBJECT
public:
	explicit SystemTray(QWidget *parent = nullptr);


private:
	QSystemTrayIcon *trayIcon;
	QMenu *trayMenu;


signals:

public slots:

private slots:
	void ShowHideWindow();
};

#endif // SYSTEMTRAY_H
